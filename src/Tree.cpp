 /*-------------------------------------------------------------------------------
 This file is part of Ranger.

 Copyright (c) [2014-2018] [Marvin N. Wright]

 This software may be modified and distributed under the terms of the MIT license.

 Please note that the C++ core of Ranger is distributed under MIT license and the
 R package "shaff" under GPL3 license.
 #-------------------------------------------------------------------------------*/

#include <iterator>

#include "Tree.h"
#include "utility.h"

namespace shaff {

Tree::Tree() :
    mtry(0), num_samples(0), num_samples_oob(0), min_node_size(0), deterministic_varIDs(0), split_select_weights(0), case_weights(
        0), manual_inbag(0), oob_sampleIDs(0), holdout(false), keep_inbag(false), data(0), regularization_factor(0), regularization_usedepth(
        false), split_varIDs_used(0), variable_importance(0), importance_mode(DEFAULT_IMPORTANCE_MODE), sample_with_replacement(
        true), sample_fraction(0), memory_saving_splitting(false), splitrule(DEFAULT_SPLITRULE), alpha(DEFAULT_ALPHA), minprop(
        DEFAULT_MINPROP), num_random_splits(DEFAULT_NUM_RANDOM_SPLITS), max_depth(DEFAULT_MAXDEPTH), depth(0), last_left_nodeID(
        0) {
}

Tree::Tree(std::vector<std::vector<size_t>>& child_nodeIDs, std::vector<size_t>& split_varIDs,
    std::vector<double>& split_values) :
    mtry(0), num_samples(0), num_samples_oob(0), min_node_size(0), deterministic_varIDs(0), split_select_weights(0), case_weights(
        0), manual_inbag(0), split_varIDs(split_varIDs), split_values(split_values), child_nodeIDs(child_nodeIDs), oob_sampleIDs(
        0), holdout(false), keep_inbag(false), data(0), regularization_factor(0), regularization_usedepth(false), split_varIDs_used(
        0), variable_importance(0), importance_mode(DEFAULT_IMPORTANCE_MODE), sample_with_replacement(true), sample_fraction(
        0), memory_saving_splitting(false), splitrule(DEFAULT_SPLITRULE), alpha(DEFAULT_ALPHA), minprop(
        DEFAULT_MINPROP), num_random_splits(DEFAULT_NUM_RANDOM_SPLITS), max_depth(DEFAULT_MAXDEPTH), depth(0), last_left_nodeID(
        0) {
}

void Tree::init(const Data* data, uint mtry, size_t num_samples, uint seed, std::vector<size_t>* deterministic_varIDs,
    std::vector<double>* split_select_weights, ImportanceMode importance_mode, uint min_node_size,
    bool sample_with_replacement, bool memory_saving_splitting, SplitRule splitrule, std::vector<double>* case_weights,
    std::vector<size_t>* manual_inbag, bool keep_inbag, std::vector<double>* sample_fraction, double alpha,
    double minprop, bool holdout, uint num_random_splits, uint max_depth, std::vector<double>* regularization_factor,
    bool regularization_usedepth, std::vector<bool>* split_varIDs_used) {

  this->data = data;
  this->mtry = mtry;
  this->num_samples = num_samples;
  this->memory_saving_splitting = memory_saving_splitting;

  // Create root node, assign bootstrap sample and oob samples
  child_nodeIDs.push_back(std::vector<size_t>());
  child_nodeIDs.push_back(std::vector<size_t>());
  child_weights.push_back(std::vector<double>());
  child_weights.push_back(std::vector<double>());
  createEmptyNode();

  // Initialize random number generator and set seed
  random_number_generator.seed(seed);

  this->deterministic_varIDs = deterministic_varIDs;
  this->split_select_weights = split_select_weights;
  this->importance_mode = importance_mode;
  this->min_node_size = min_node_size;
  this->sample_with_replacement = sample_with_replacement;
  this->splitrule = splitrule;
  this->case_weights = case_weights;
  this->manual_inbag = manual_inbag;
  this->keep_inbag = keep_inbag;
  this->sample_fraction = sample_fraction;
  this->holdout = holdout;
  this->alpha = alpha;
  this->minprop = minprop;
  this->num_random_splits = num_random_splits;
  this->max_depth = max_depth;
  this->regularization_factor = regularization_factor;
  this->regularization_usedepth = regularization_usedepth;
  this->split_varIDs_used = split_varIDs_used;

  // Regularization
  if (regularization_factor->size() > 0) {
    regularization = true;
  } else {
    regularization = false;
  }
}

void Tree::grow(std::vector<double>* variable_importance) {
  // Allocate memory for tree growing
  allocateMemory();

  this->variable_importance = variable_importance;

  // Bootstrap, dependent if weighted or not and with or without replacement
  if (!case_weights->empty()) {
    if (sample_with_replacement) {
      bootstrapWeighted();
    } else {
      bootstrapWithoutReplacementWeighted();
    }
  } else if (sample_fraction->size() > 1) {
    if (sample_with_replacement) {
      bootstrapClassWise();
    } else {
      bootstrapWithoutReplacementClassWise();
    }
  } else if (!manual_inbag->empty()) {
    setManualInbag();
  } else {
    if (sample_with_replacement) {
      bootstrap();
    } else {
      bootstrapWithoutReplacement();
    }
  }

  // Init start and end positions
  start_pos[0] = 0;
  end_pos[0] = sampleIDs.size();

  // While not all nodes terminal, split next node
  size_t num_open_nodes = 1;
  size_t i = 0;
  depth = 0;
  while (num_open_nodes > 0) {
    // Split node
    bool is_terminal_node = splitNode(i);
    if (is_terminal_node) {
      --num_open_nodes;
    } else {
      ++num_open_nodes;
      if (i >= last_left_nodeID) {
        // If new level, increase depth
        // (left_node saves left-most node in current level, new level reached if that node is splitted)
        last_left_nodeID = split_varIDs.size() - 2;
        ++depth;
      }
    }
    ++i;
  }

  // Delete sampleID vector to save memory
  // sampleIDs.clear();
  // sampleIDs.shrink_to_fit();
  cleanUpInternal();
}

void Tree::predict(const Data* prediction_data, bool oob_prediction) {

  size_t num_samples_predict;
  if (oob_prediction) {
    num_samples_predict = num_samples_oob;
  } else {
    num_samples_predict = prediction_data->getNumRows();
  }

  prediction_terminal_nodeIDs.resize(num_samples_predict, 0);

  // For each sample start in root, drop down the tree and return final value
  for (size_t i = 0; i < num_samples_predict; ++i) {
    size_t sample_idx;
    if (oob_prediction) {
      sample_idx = oob_sampleIDs[i];
    } else {
      sample_idx = i;
    }
    size_t nodeID = 0;
    while (1) {

      // Break if terminal node
      if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
        break;
      }

      // Move to child
      size_t split_varID = split_varIDs[nodeID];

      double value = prediction_data->get_x(sample_idx, split_varID);
      if (prediction_data->isOrderedVariable(split_varID)) {
        if (value <= split_values[nodeID]) {
          // Move to left child
          nodeID = child_nodeIDs[0][nodeID];
        } else {
          // Move to right child
          nodeID = child_nodeIDs[1][nodeID];
        }
      } else {
        size_t factorID = floor(value) - 1;
        size_t splitID = floor(split_values[nodeID]);

        // Left if 0 found at position factorID
        if (!(splitID & (1ULL << factorID))) {
          // Move to left child
          nodeID = child_nodeIDs[0][nodeID];
        } else {
          // Move to right child
          nodeID = child_nodeIDs[1][nodeID];
        }
      }
    }

    prediction_terminal_nodeIDs[i] = nodeID;
  }
}

void Tree::computePermutationImportance(std::vector<double>& forest_importance, std::vector<double>& forest_variance,
    std::vector<double>& forest_importance_casewise) {

  size_t num_independent_variables = data->getNumCols();

  // Compute normal prediction accuracy for each tree. Predictions already computed..
  double accuracy_normal;
  std::vector<double> prederr_normal_casewise;
  std::vector<double> prederr_shuf_casewise;
  if (importance_mode == IMP_PERM_CASEWISE) {
    prederr_normal_casewise.resize(num_samples_oob, 0);
    prederr_shuf_casewise.resize(num_samples_oob, 0);
    accuracy_normal = computePredictionAccuracyInternal(&prederr_normal_casewise);
  } else {
    accuracy_normal = computePredictionAccuracyInternal(NULL);
  }

  prediction_terminal_nodeIDs.clear();
  prediction_terminal_nodeIDs.resize(num_samples_oob, 0);

  // Reserve space for permutations, initialize with oob_sampleIDs
  std::vector<size_t> permutations(oob_sampleIDs);

  // Randomly permute for all independent variables
  for (size_t i = 0; i < num_independent_variables; ++i) {

    // Permute and compute prediction accuracy again for this permutation and save difference
    permuteAndPredictOobSamples(i, permutations);
    double accuracy_permuted;
    if (importance_mode == IMP_PERM_CASEWISE) {
      accuracy_permuted = computePredictionAccuracyInternal(&prederr_shuf_casewise);
      for (size_t j = 0; j < num_samples_oob; ++j) {
        size_t pos = i * num_samples + oob_sampleIDs[j];
        forest_importance_casewise[pos] += prederr_shuf_casewise[j] - prederr_normal_casewise[j];
      }
    } else {
      accuracy_permuted = computePredictionAccuracyInternal(NULL);
    }

    double accuracy_difference = accuracy_normal - accuracy_permuted;
    forest_importance[i] += accuracy_difference;

    // Compute variance
    if (importance_mode == IMP_PERM_BREIMAN) {
      forest_variance[i] += accuracy_difference * accuracy_difference;
    } else if (importance_mode == IMP_PERM_LIAW) {
      forest_variance[i] += accuracy_difference * accuracy_difference * num_samples_oob;
    }
  }
}

std::vector<double> Tree::predictOobWeightedVarU(std::vector<size_t> varU, bool Ucomplement, const Data* prediction_data) {
  
  // Initialize variables
  std::map<size_t, std::vector<size_t>> nodeIDs_samplesIDs_inb;
  // std::map<std::vector<size_t>, std::vector<size_t>>::iterator it;
  std::pair<std::map<size_t, std::vector<size_t>>::iterator, bool> it_bool;
  std::vector<size_t> sampleIDs_unique;
  std::map<size_t, size_t> sampleIDs_count;
  std::pair<std::map<size_t, size_t>::iterator, bool> itIDs; 
  for (auto & i : sampleIDs){
    itIDs = sampleIDs_count.insert(std::pair<size_t, size_t> (i, 1));
    if (!itIDs.second){
      itIDs.first->second += 1;
    }else{
      sampleIDs_unique.push_back(i);
    }
  }
  std::vector<size_t> nodeIDroot = {0};
  std::vector<size_t> nodeIDtemp;
  std::vector<size_t> nodeIDnext;
  std::vector<size_t> nodeIDpred;
  std::vector<size_t> nodeIDlevel;
  std::vector<size_t> nodeIDpredtemp;
  bool inU;
  
  // For each in-bag sample start in root, drop down the tree and return node set at each level
  for (auto & i : sampleIDs_unique) {
    
    nodeIDtemp = nodeIDroot;
    nodeIDnext = {};
    nodeIDpred = {};
    
    while (nodeIDtemp.size() > 0) {
      
      for (auto & nodeID : nodeIDtemp){
        if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
          nodeIDpred.push_back(nodeID);
        }else{
          size_t split_varID = split_varIDs[nodeID];
          // tree path depends whether splitting variable is in varU or not
          inU = find(varU.begin(), varU.end(), split_varID) != varU.end();
          if (Ucomplement){
            inU = !inU;
          }
          if (inU){
            // Move to child if splitting variable is in varU
            double value = prediction_data->get_x(i, split_varID);
            if (prediction_data->isOrderedVariable(split_varID)) {
              if (value <= split_values[nodeID]) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            } else {
              size_t factorID = floor(value) - 1;
              size_t splitID = floor(split_values[nodeID]);
              // Left if 0 found at position factorID
              if (!(splitID & (1ULL << factorID))) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            }
          }else{
            // When splitting on variable not in varU, move to both left and right children
            nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
            nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
          }
        }
      }
      
      nodeIDtemp = nodeIDnext;
      nodeIDnext = {};
      
    }
    
    for (auto & nodeID : nodeIDpred){
      it_bool = nodeIDs_samplesIDs_inb.insert(std::pair<size_t, std::vector<size_t>> (nodeID, {i}));
      if (!it_bool.second){
        it_bool.first->second.push_back(i);
      }
    }
    
  }
  
  // Recompute output average in each terminal cell of the tree
  std::map<size_t, double> nodeIDs_meanY;
  size_t node_size;
  for (std::map<size_t, std::vector<size_t>>::iterator it = nodeIDs_samplesIDs_inb.begin(); it != nodeIDs_samplesIDs_inb.end(); ++it){
    nodeIDs_meanY[it->first] = 0;
    node_size = 0;
    for (auto & i : it->second){
      nodeIDs_meanY[it->first] += prediction_data->get_y(i, 0)*sampleIDs_count[i];
      node_size += sampleIDs_count[i];
    }
    nodeIDs_meanY[it->first] = nodeIDs_meanY[it->first]/node_size;
  }

  // For each out-of-bag sample start in root, drop down the tree and return weighted cell output
  int nsample = prediction_data->getNumRows();
  std::vector<double> pred_oob(nsample, 0);
  for (auto & i : oob_sampleIDs) {

    nodeIDtemp = nodeIDroot;
    nodeIDnext = {};
    nodeIDpred = {};
    std::vector<double> weights;
    std::vector<double> weights_temp = {1};
    std::vector<double> weights_next;
    
    while (nodeIDtemp.size() > 0) {
      
      // for (auto & nodeID : nodeIDtemp){
      for (size_t t = 0; t < nodeIDtemp.size(); ++t){
        size_t nodeID = nodeIDtemp[t];
        double weight = weights_temp[t];
        if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
          nodeIDpred.push_back(nodeID);
          weights.push_back(weight);
        }else{
          size_t split_varID = split_varIDs[nodeID];
          inU = find(varU.begin(), varU.end(), split_varID) != varU.end();
          if (Ucomplement){
            inU = !inU;
          }
          // tree path depends whether splitting variable is in varU or not
          if (inU){
            // Move to child if splitting variable is in varU
            double value = prediction_data->get_x(i, split_varID);
            if (prediction_data->isOrderedVariable(split_varID)) {
              if (value <= split_values[nodeID]) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            } else {
              size_t factorID = floor(value) - 1;
              size_t splitID = floor(split_values[nodeID]);
              // Left if 0 found at position factorID
              if (!(splitID & (1ULL << factorID))) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            }
            weights_next.push_back(weight);
          }else{
            // When splitting on variable not in varU, move to both left and right children
            nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
            nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
            weights_next.push_back(weight*child_weights[0][nodeID]);
            weights_next.push_back(weight*child_weights[1][nodeID]);
          }
        }
      }
      
      nodeIDtemp = nodeIDnext;
      nodeIDnext = {};
      weights_temp = weights_next;
      weights_next = {};
      
    }
    
    //for (auto & nodeID : nodeIDpred){
    for (size_t k = 0; k < nodeIDpred.size(); ++k){
      pred_oob[i] += nodeIDs_meanY[nodeIDpred[k]] * weights[k];
    }
    //pred_oob[i] = pred_oob[i]/nodeIDpred.size();
  
  }
  
  return(pred_oob);
  
}

std::vector<double> Tree::predictOobTreeSHAPVarU(std::vector<size_t> varU, bool Ucomplement, const Data* prediction_data) {
  
  // Initialize variables
  std::map<size_t, std::vector<size_t>> nodeIDs_samplesIDs_inb;
  std::pair<std::map<size_t, std::vector<size_t>>::iterator, bool> it_bool;
  std::map<size_t, size_t> sampleIDs_count;
  std::pair<std::map<size_t, size_t>::iterator, bool> itIDs; 
  std::vector<size_t> nodeIDroot = {0};
  std::vector<size_t> nodeIDtemp;
  std::vector<size_t> nodeIDnext;
  std::vector<size_t> nodeIDpred;
  std::vector<size_t> nodeIDlevel;
  std::vector<size_t> nodeIDpredtemp;
  bool inU;
  
  // For each out-of-bag sample start in root, drop down the tree and return weighted cell output
  int nsample = prediction_data->getNumRows();
  std::vector<double> pred_oob(nsample, 0);
  for (auto & i : oob_sampleIDs) {
    
    nodeIDtemp = nodeIDroot;
    nodeIDnext = {};
    nodeIDpred = {};
    std::vector<double> weights;
    std::vector<double> weights_temp = {1};
    std::vector<double> weights_next;
    
    while (nodeIDtemp.size() > 0) {
      
      for (size_t t = 0; t < nodeIDtemp.size(); ++t){
        size_t nodeID = nodeIDtemp[t];
        double weight = weights_temp[t];
        if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
          nodeIDpred.push_back(nodeID);
          weights.push_back(weight);
        }else{
          size_t split_varID = split_varIDs[nodeID];
          inU = find(varU.begin(), varU.end(), split_varID) != varU.end();
          if (Ucomplement){
            inU = !inU;
          }
          // tree path depends whether splitting variable is in varU or not
          if (inU){
            // Move to child if splitting variable is in varU
            double value = prediction_data->get_x(i, split_varID);
            if (prediction_data->isOrderedVariable(split_varID)) {
              if (value <= split_values[nodeID]) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            } else {
              size_t factorID = floor(value) - 1;
              size_t splitID = floor(split_values[nodeID]);
              // Left if 0 found at position factorID
              if (!(splitID & (1ULL << factorID))) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            }
            weights_next.push_back(weight);
          }else{
            // When splitting on variable not in varU, move to both left and right children
            nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
            nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
            weights_next.push_back(weight*child_weights[0][nodeID]);
            weights_next.push_back(weight*child_weights[1][nodeID]);
          }
        }
      }
      
      nodeIDtemp = nodeIDnext;
      nodeIDnext = {};
      weights_temp = weights_next;
      weights_next = {};
      
    }
    
    for (size_t k = 0; k < nodeIDpred.size(); ++k){
      pred_oob[i] += split_values[nodeIDpred[k]] * weights[k];
    }
    
  }
  
  return(pred_oob);
  
}

std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>> Tree::getPrunedChildNodes(std::vector<std::vector<size_t>> UlistMC){
 
  size_t num_nodes = child_nodeIDs[0].size();
  std::vector<size_t> parent_nodeIDs(num_nodes);
  std::vector<size_t> terminal_nodeIDs;
  std::vector<size_t> terminal_nodeIDs_temp;
  std::vector<size_t> terminal_nodeIDs_next;
  std::pair<std::vector<size_t>, std::vector<size_t>> child_varU;
  std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>> child_UlistMC;
  
  for (size_t nodeID = 0; nodeID < num_nodes; ++nodeID){
    parent_nodeIDs[child_nodeIDs[0][nodeID]] = nodeID;
    parent_nodeIDs[child_nodeIDs[1][nodeID]] = nodeID;
    if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
      terminal_nodeIDs.push_back(nodeID);
    }
  }
  
  for (auto & varU : UlistMC){
    
    terminal_nodeIDs_temp = terminal_nodeIDs;
    std::vector<size_t> child_nodeIDs_pruned_left = child_nodeIDs[0];
    std::vector<size_t> child_nodeIDs_pruned_right = child_nodeIDs[1];
    std::vector<size_t> parent_delete(num_nodes, 0);
    
    while (terminal_nodeIDs_temp.size() > 0){
      for (auto & nodeID : terminal_nodeIDs_temp){
        size_t parentID = parent_nodeIDs[nodeID];
        size_t split_varID = split_varIDs[parentID];
        bool outU = find(varU.begin(), varU.end(), split_varID) == varU.end();
        if (outU){
          parent_delete[parentID] += 1;
          if (parent_delete[parentID] == 2){
            child_nodeIDs_pruned_left[parentID] = 0;
            child_nodeIDs_pruned_right[parentID] = 0;
            terminal_nodeIDs_next.push_back(parentID);
          }
        }
      }
      terminal_nodeIDs_temp = terminal_nodeIDs_next;
      terminal_nodeIDs_next.clear();
    }
    
    child_varU.first = child_nodeIDs_pruned_left;
    child_varU.second = child_nodeIDs_pruned_right;
    child_UlistMC.push_back(child_varU);
    
  }
  
  return(child_UlistMC);

}

std::vector<std::pair<std::vector<size_t>, std::vector<double>>> Tree::predictOobProjectedFastMC(
    std::vector<std::vector<size_t>> UlistMC, bool Ucomplement, size_t tree_depth, const Data* prediction_data){
  
  
  // Initialize variables
  std::vector<size_t> sampleIDs_unique;
  std::map<size_t, size_t> sampleIDs_count;
  std::pair<std::map<size_t, size_t>::iterator, bool> itIDs; 
  std::sort(sampleIDs.begin(), sampleIDs.end());
  std::sort(oob_sampleIDs.begin(), oob_sampleIDs.end());
  // Deduplicate boostrapped training data
  for (auto & i : sampleIDs){
    itIDs = sampleIDs_count.insert(std::pair<size_t, size_t> (i, 1));
    if (!itIDs.second){
      itIDs.first->second += 1;
    }else{
      sampleIDs_unique.push_back(i);
    }
  }
  
  
  // Split the full dataset at each node
  std::vector<std::vector<size_t>> sampleIDs_left_inb;
  std::vector<std::vector<size_t>> sampleIDs_right_inb;
  std::vector<std::vector<size_t>> sampleIDs_left_oob;
  std::vector<std::vector<size_t>> sampleIDs_right_oob;
  std::vector<size_t> sampleIDs_left_temp;
  std::vector<size_t> sampleIDs_right_temp;
  
  
  // Initialize variables
  // Maps to define projected partitions: key is the nodeID sequence of the projected cell, value is 
  // the pair of INB/OOB samples in the cell.
  size_t num_varU = UlistMC.size();
  std::vector<std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<size_t>>>> nodeIDs_varIDs_sampleIDs;
  std::vector<std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<size_t>>>> nodeIDs_varIDs_sampleIDs_terminal(num_varU);
  std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_temp;
  std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_level;
  std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_U;
  std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_next;
  std::pair<std::vector<size_t>, std::vector<size_t>> U_sampleIDs;
  std::pair<std::vector<size_t>, std::vector<size_t>> U_sampleIDs_left;
  std::pair<std::vector<size_t>, std::vector<size_t>> U_sampleIDs_right;
  std::pair<std::vector<size_t>, std::vector<size_t>> U_sampleIDs_current;
  std::vector<size_t> sampleIDs_temp_inb;
  std::vector<size_t> sampleIDs_temp_oob;
  std::vector<size_t> varIDs;
  std::vector<size_t> nodeIDs;
  std::vector<size_t> nodeIDs_left;
  std::vector<size_t> nodeIDs_right;
  std::vector<size_t> nodeIDs_level;
  std::vector<size_t> nodeIDs_level_next;
  std::vector<std::vector<size_t>> nodeIDs_next;
  std::vector<std::vector<size_t>> nodeIDs_temp;
  size_t split_varID;
  
  
  // get pruned trees
  std::vector<size_t> child_nodeIDs_left;
  std::vector<size_t> child_nodeIDs_right;
  std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>> child_pruned_UlistMC;
  if (!Ucomplement){
    child_pruned_UlistMC = getPrunedChildNodes(UlistMC);
  }

  // Initialize with first tree level
  nodeIDs.push_back(0);
  nodeIDs_level = nodeIDs;
  U_sampleIDs.first = sampleIDs_unique;
  U_sampleIDs.second = oob_sampleIDs;
  nodeIDs_varIDs_sampleIDs_U[nodeIDs] = U_sampleIDs;
  for (auto & U : UlistMC){
    nodeIDs_varIDs_sampleIDs.push_back(nodeIDs_varIDs_sampleIDs_U);
  }
  nodeIDs.clear();
  size_t tree_level = 0;
  size_t min_terminal_size = 5;

  // loop through all tree levels to compute projected partition
  while (tree_level < tree_depth){
    // while (nodeIDs_varIDs_sampleIDs.size() > 0){

    // Split the full dataset at each node of the current tree level
    for (auto & nodeID : nodeIDs_level){

      if (child_nodeIDs[0][nodeID] != 0 && child_nodeIDs[1][nodeID] != 0) {
        nodeIDs_level_next.push_back(child_nodeIDs[0][nodeID]);
        nodeIDs_level_next.push_back(child_nodeIDs[1][nodeID]);
      }

      size_t split_varID = split_varIDs[nodeID];
      double split_value = split_values[nodeID];

      // in-bag sample
      for (auto & i : sampleIDs_unique) {
        double value = prediction_data->get_x(i, split_varID);
        if (prediction_data->isOrderedVariable(split_varID)) {
          if (value <= split_value) {
            // Move to left child
            sampleIDs_left_temp.push_back(i);
          } else {
            // Move to right child
            sampleIDs_right_temp.push_back(i);
          }
        } else {
          size_t factorID = floor(value) - 1;
          size_t splitID = floor(split_value);
          // Left if 0 found at position factorID
          if (!(splitID & (1ULL << factorID))) {
            // Move to left child
            sampleIDs_left_temp.push_back(i);
          } else {
            // Move to right child
            sampleIDs_right_temp.push_back(i);
          }
        }
      }
      sampleIDs_left_inb.push_back(sampleIDs_left_temp);
      sampleIDs_right_inb.push_back(sampleIDs_right_temp);
      sampleIDs_left_temp.clear();
      sampleIDs_right_temp.clear();

      // oob-bag sample
      for (auto & i : oob_sampleIDs) {
        double value = prediction_data->get_x(i, split_varID);
        if (prediction_data->isOrderedVariable(split_varID)) {
          if (value <= split_value) {
            // Move to left child
            sampleIDs_left_temp.push_back(i);
          } else {
            // Move to right child
            sampleIDs_right_temp.push_back(i);
          }
        } else {
          size_t factorID = floor(value) - 1;
          size_t splitID = floor(split_value);
          // Left if 0 found at position factorID
          if (!(splitID & (1ULL << factorID))) {
            // Move to left child
            sampleIDs_left_temp.push_back(i);
          } else {
            // Move to right child
            sampleIDs_right_temp.push_back(i);
          }
        }
      }
      sampleIDs_left_oob.push_back(sampleIDs_left_temp);
      sampleIDs_right_oob.push_back(sampleIDs_right_temp);
      sampleIDs_left_temp.clear();
      sampleIDs_right_temp.clear();

    }

    nodeIDs_level = nodeIDs_level_next;
    nodeIDs_level_next.clear();

    // compute projected partitions at the current tree level
    for (size_t k = 0; k < num_varU; ++k){

      nodeIDs_varIDs_sampleIDs_U = nodeIDs_varIDs_sampleIDs[k];
      varIDs = UlistMC[k];
      if (Ucomplement){
        child_nodeIDs_left = child_nodeIDs[0];
        child_nodeIDs_right = child_nodeIDs[1];
      } else{
        child_nodeIDs_left = child_pruned_UlistMC[k].first;
        child_nodeIDs_right = child_pruned_UlistMC[k].second;
      }

      for (std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<size_t>>>::iterator it = nodeIDs_varIDs_sampleIDs_U.begin();
           it != nodeIDs_varIDs_sampleIDs_U.end(); ++it){

        sampleIDs_temp_inb = it->second.first;
        sampleIDs_temp_oob = it->second.second;
        nodeIDs_temp.push_back({});
        bool allTerminal = true;
        bool inU;

        // TO DO randomized nodeID order
        for (auto & nodeID : it->first){

          bool isTerminal = false;
          if (child_nodeIDs_left[nodeID] == 0 && child_nodeIDs_right[nodeID] == 0) {
            isTerminal = true;
          }else{
            split_varID = split_varIDs[nodeID];
            inU = find(varIDs.begin(), varIDs.end(), split_varID) != varIDs.end();
            if (Ucomplement){
              inU = !inU;
            }
          }
          allTerminal = allTerminal && isTerminal;

          for (auto & nodeID_current : nodeIDs_temp){

            if (nodeID_current.size() > 0){
              nodeIDs = nodeID_current;
              nodeIDs_left = nodeID_current;
              nodeIDs_right = nodeID_current;
              U_sampleIDs_current = nodeIDs_varIDs_sampleIDs_temp[nodeID_current];
              sampleIDs_temp_inb = U_sampleIDs_current.first;
              sampleIDs_temp_oob = U_sampleIDs_current.second;
            }

            if (isTerminal){

              // nodeIDs.push_back(nodeID);
              nodeIDs_next.push_back(nodeIDs);
              U_sampleIDs.first = sampleIDs_temp_inb;
              U_sampleIDs.second = sampleIDs_temp_oob;
              nodeIDs_varIDs_sampleIDs_next[nodeIDs] = U_sampleIDs;

            } else{

              if (inU){
                // nodeIDs
                nodeIDs_left.push_back(child_nodeIDs_left[nodeID]);
                nodeIDs_right.push_back(child_nodeIDs_right[nodeID]);
                // sampleIDs in-bag
                set_intersection(sampleIDs_temp_inb.begin(), sampleIDs_temp_inb.end(), sampleIDs_left_inb[nodeID].begin(),
                                 sampleIDs_left_inb[nodeID].end(), std::back_inserter(sampleIDs_left_temp));
                set_intersection(sampleIDs_temp_inb.begin(), sampleIDs_temp_inb.end(), sampleIDs_right_inb[nodeID].begin(),
                                 sampleIDs_right_inb[nodeID].end(), std::back_inserter(sampleIDs_right_temp));
                U_sampleIDs_left.first = sampleIDs_left_temp;
                U_sampleIDs_right.first = sampleIDs_right_temp;
                // stop if cell is empty
                bool valid_cell_left_inb = sampleIDs_left_temp.size() > min_terminal_size;
                bool valid_cell_right_inb = sampleIDs_right_temp.size() > min_terminal_size;
                sampleIDs_left_temp.clear();
                sampleIDs_right_temp.clear();
                // sampleIDs oob-bag
                set_intersection(sampleIDs_temp_oob.begin(), sampleIDs_temp_oob.end(), sampleIDs_left_oob[nodeID].begin(),
                                 sampleIDs_left_oob[nodeID].end(), std::back_inserter(sampleIDs_left_temp));
                set_intersection(sampleIDs_temp_oob.begin(), sampleIDs_temp_oob.end(), sampleIDs_right_oob[nodeID].begin(),
                                 sampleIDs_right_oob[nodeID].end(), std::back_inserter(sampleIDs_right_temp));
                U_sampleIDs_left.second = sampleIDs_left_temp;
                U_sampleIDs_right.second = sampleIDs_right_temp;
                // stop if cell is empty
                bool valid_cell_left_oob = sampleIDs_left_temp.size() > 0;
                bool valid_cell_right_oob = sampleIDs_right_temp.size() > 0;
                sampleIDs_left_temp.clear();
                sampleIDs_right_temp.clear();
                // store new cells
                if (valid_cell_left_oob){
                  if (valid_cell_left_inb){
                    nodeIDs_varIDs_sampleIDs_next[nodeIDs_left] = U_sampleIDs_left;
                    nodeIDs_next.push_back(nodeIDs_left);
                  } else{
                    U_sampleIDs_left.first = sampleIDs_temp_inb;
                    U_sampleIDs_left.second = sampleIDs_temp_oob;
                    nodeIDs_varIDs_sampleIDs_terminal[k][nodeIDs_left] = U_sampleIDs_left;
                  }
                }
                if (valid_cell_right_oob){
                  if (valid_cell_right_inb){
                    nodeIDs_varIDs_sampleIDs_next[nodeIDs_right] = U_sampleIDs_right;
                    nodeIDs_next.push_back(nodeIDs_right);
                  } else{
                    U_sampleIDs_right.first = sampleIDs_temp_inb;
                    U_sampleIDs_right.second = sampleIDs_temp_oob;
                    nodeIDs_varIDs_sampleIDs_terminal[k][nodeIDs_right] = U_sampleIDs_right;
                  }
                }
              } else{
                // nodeIDs
                nodeIDs.push_back(child_nodeIDs_left[nodeID]);
                nodeIDs.push_back(child_nodeIDs_right[nodeID]);
                nodeIDs_next.push_back(nodeIDs);
                // sampleIDs
                U_sampleIDs.first = sampleIDs_temp_inb;
                U_sampleIDs.second = sampleIDs_temp_oob;
                // store new cell
                nodeIDs_varIDs_sampleIDs_next[nodeIDs] = U_sampleIDs;
              }

            }

            // clean
            sampleIDs_temp_inb.clear();
            sampleIDs_temp_oob.clear();
            nodeIDs_left.clear();
            nodeIDs_right.clear();
            nodeIDs.clear();

          }

          nodeIDs_varIDs_sampleIDs_temp = nodeIDs_varIDs_sampleIDs_next;
          nodeIDs_varIDs_sampleIDs_next.clear();
          nodeIDs_temp = nodeIDs_next;
          nodeIDs_next.clear();

        }

        if (allTerminal){
          nodeIDs_varIDs_sampleIDs_terminal[k][it->first] = it->second;
        }

        if (!allTerminal){
          nodeIDs_varIDs_sampleIDs_level.insert(nodeIDs_varIDs_sampleIDs_temp.begin(), nodeIDs_varIDs_sampleIDs_temp.end());
        }
        nodeIDs_varIDs_sampleIDs_temp.clear();
        nodeIDs_temp.clear();

      }

      nodeIDs_varIDs_sampleIDs[k] = nodeIDs_varIDs_sampleIDs_level;
      nodeIDs_varIDs_sampleIDs_level.clear();

    }

    tree_level += 1;

  }

  for (size_t k = 0; k < num_varU; ++k){
    nodeIDs_varIDs_sampleIDs_terminal[k].insert(nodeIDs_varIDs_sampleIDs[k].begin(), nodeIDs_varIDs_sampleIDs[k].end());
  }
  
  // debug
  // std::vector<std::vector<std::vector<std::vector<size_t>>>> res;
  // std::vector<std::vector<std::vector<size_t>>> res_U;
  // std::vector<std::vector<size_t>> res_temp;
  // for (size_t k = 0; k < num_varU; ++k){
  //   for (std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<size_t>>>::iterator it = nodeIDs_varIDs_sampleIDs_terminal[k].begin();
  //        it != nodeIDs_varIDs_sampleIDs_terminal[k].end(); ++it){
  //     res_temp.push_back(it->second.first);
  //     res_temp.push_back(it->second.second);
  //     res_U.push_back(res_temp);
  //     res_temp.clear();
  //   }
  //   res.push_back(res_U);
  //   res_U.clear();
  // }
  // 
  // return(res);
  
  // debug
  // std::vector<std::vector<std::vector<std::vector<double>>>> res;
  // std::vector<std::vector<std::vector<double>>> res_U;
  // std::vector<std::vector<double>> res_temp;
  // std::vector<double> res_temp1;
  // std::vector<double> res_temp2;

  // compute predictions
  std::vector<std::pair<std::vector<size_t>, std::vector<double>>> predictions;
  std::pair<std::vector<size_t>, std::vector<double>> Upred;
  double cell_mean;
  size_t nsample = prediction_data->getNumRows();
  std::vector<double> pred_null(nsample, 0);

  for (size_t k = 0; k < num_varU; ++k){

    // varIDs = UlistMC[k];
    Upred.second = pred_null;
    // Upred.first = oob_sampleIDs;
    Upred.first.clear();

    for (std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<size_t>>>::iterator it = nodeIDs_varIDs_sampleIDs_terminal[k].begin();
         it != nodeIDs_varIDs_sampleIDs_terminal[k].end(); ++it){
      double cell_mean = 0;
      size_t cell_size = 0;
      for (auto & i : it->second.first){
        cell_mean += prediction_data->get_y(i, 0)*sampleIDs_count[i];
        cell_size += sampleIDs_count[i];
        // res_temp1.push_back(prediction_data->get_y(i, 0));
        // res_temp2.push_back(static_cast<double> (sampleIDs_count[i]));
      }
      cell_mean = cell_mean / cell_size;
      
      for (auto & i : it->second.second){
        Upred.second[i] = cell_mean;
        Upred.first.push_back(i);
        // Upred.first.push_back(static_cast<double> (i));
      }
      
    }
    
    // // debug
    // // res_temp.push_back(res_temp1);
    // // res_temp.push_back(res_temp2);
    // // res_temp.push_back({cell_mean});
    // res_temp.push_back(Upred.second);
    // // res_temp.push_back(Upred.first);
    // res_U.push_back(res_temp);
    // res_temp1.clear();
    // res_temp2.clear();
    // res_temp.clear();
    // res.push_back(res_U);
    // res_U.clear();
    
    predictions.push_back(Upred);

  }

  // return(res);
  return(predictions);
  
}

std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<double>>> Tree::predictOobProjectedMC(
    std::vector<std::vector<size_t>> UlistMC, bool Ucomplement, size_t tree_depth, const Data* prediction_data){
  
  
  // Initialize variables
  std::vector<size_t> sampleIDs_unique;
  std::map<size_t, size_t> sampleIDs_count;
  std::pair<std::map<size_t, size_t>::iterator, bool> itIDs; 
  std::sort(sampleIDs.begin(), sampleIDs.end());
  std::sort(oob_sampleIDs.begin(), oob_sampleIDs.end());
  // Deduplicate boostrapped training data
  for (auto & i : sampleIDs){
    itIDs = sampleIDs_count.insert(std::pair<size_t, size_t> (i, 1));
    if (!itIDs.second){
      itIDs.first->second += 1;
    }else{
      sampleIDs_unique.push_back(i);
    }
  }
  
  
  // Split the full dataset at each node
  std::vector<std::vector<size_t>> sampleIDs_left_inb;
  std::vector<std::vector<size_t>> sampleIDs_right_inb;
  std::vector<std::vector<size_t>> sampleIDs_left_oob;
  std::vector<std::vector<size_t>> sampleIDs_right_oob;
  std::vector<size_t> sampleIDs_left_temp;
  std::vector<size_t> sampleIDs_right_temp;
  
  
  // Initialize variables
  // Maps to define projected partitions: key is the nodeID sequence of the projected cell, value is 
  // a list of vectors containing the list of splitting/ignored variables and the INB/OOB samples of the cell.
  std::vector<std::map<std::vector<size_t>, std::vector<std::vector<size_t>>>> levels_nodeIDs_varIDs_sampleIDs;
  std::map<std::vector<size_t>, std::vector<std::vector<size_t>>> nodeIDs_varIDs_sampleIDs;
  std::map<std::vector<size_t>, std::vector<std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_temp;
  std::map<std::vector<size_t>, std::vector<std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_next;
  std::map<std::vector<size_t>, std::vector<std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_level;
  std::map<std::vector<size_t>, std::vector<std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_terminal;
  std::vector<std::vector<size_t>> varIDs_sampleIDs;
  std::vector<std::vector<size_t>> varIDs_sampleIDs_left;
  std::vector<std::vector<size_t>> varIDs_sampleIDs_right;
  std::vector<std::vector<size_t>> varIDs_sampleIDs_current;
  std::vector<size_t> sampleIDs_temp_inb;
  std::vector<size_t> sampleIDs_temp_oob;
  std::vector<size_t> varIDs;
  std::vector<size_t> varIDsOut;
  std::vector<size_t> varIDs_next;
  std::vector<size_t> varIDs_check;
  std::vector<size_t> varIDsOut_next;
  std::vector<size_t> nodeIDs;
  std::vector<size_t> nodeIDs_level;
  std::vector<size_t> nodeIDs_level_next;
  std::vector<size_t> nodeIDs_left;
  std::vector<size_t> nodeIDs_right;
  std::vector<std::vector<size_t>> nodeIDs_temp;
  std::vector<std::vector<size_t>> nodeIDs_next;
  size_t split_varID;
  std::vector<size_t> U;
  
  
  // Initialize with first tree level
  nodeIDs.push_back(0);
  nodeIDs_level = nodeIDs;
  varIDs_sampleIDs.push_back(varIDs);
  varIDs_sampleIDs.push_back(varIDsOut);
  varIDs_sampleIDs.push_back(sampleIDs_unique);
  varIDs_sampleIDs.push_back(oob_sampleIDs);
  nodeIDs_varIDs_sampleIDs[nodeIDs] = varIDs_sampleIDs;
  nodeIDs.clear();
  varIDs_sampleIDs.clear();
  size_t tree_level = 0;
  size_t min_terminal_size = 5;
  
  
  // loop through all tree levels to compute projected partition
  while (tree_level < tree_depth){
    // while (nodeIDs_varIDs_sampleIDs.size() > 0){
    
    // Split the full dataset at each node of the current tree level 
    for (auto & nodeID : nodeIDs_level){
      
      if (child_nodeIDs[0][nodeID] != 0 && child_nodeIDs[1][nodeID] != 0) {
        nodeIDs_level_next.push_back(child_nodeIDs[0][nodeID]);
        nodeIDs_level_next.push_back(child_nodeIDs[1][nodeID]);
      }
      
      size_t split_varID = split_varIDs[nodeID];
      double split_value = split_values[nodeID];
      
      // in-bag sample
      for (auto & i : sampleIDs_unique) {
        double value = prediction_data->get_x(i, split_varID);
        if (prediction_data->isOrderedVariable(split_varID)) {
          if (value <= split_value) {
            // Move to left child
            sampleIDs_left_temp.push_back(i);
          } else {
            // Move to right child
            sampleIDs_right_temp.push_back(i);
          }
        } else {
          size_t factorID = floor(value) - 1;
          size_t splitID = floor(split_value);
          // Left if 0 found at position factorID
          if (!(splitID & (1ULL << factorID))) {
            // Move to left child
            sampleIDs_left_temp.push_back(i);
          } else {
            // Move to right child
            sampleIDs_right_temp.push_back(i);
          }
        }
      }
      sampleIDs_left_inb.push_back(sampleIDs_left_temp);
      sampleIDs_right_inb.push_back(sampleIDs_right_temp);
      sampleIDs_left_temp.clear();
      sampleIDs_right_temp.clear();
      
      // oob-bag sample
      for (auto & i : oob_sampleIDs) {
        double value = prediction_data->get_x(i, split_varID);
        if (prediction_data->isOrderedVariable(split_varID)) {
          if (value <= split_value) {
            // Move to left child
            sampleIDs_left_temp.push_back(i);
          } else {
            // Move to right child
            sampleIDs_right_temp.push_back(i);
          }
        } else {
          size_t factorID = floor(value) - 1;
          size_t splitID = floor(split_value);
          // Left if 0 found at position factorID
          if (!(splitID & (1ULL << factorID))) {
            // Move to left child
            sampleIDs_left_temp.push_back(i);
          } else {
            // Move to right child
            sampleIDs_right_temp.push_back(i);
          }
        }
      }
      sampleIDs_left_oob.push_back(sampleIDs_left_temp);
      sampleIDs_right_oob.push_back(sampleIDs_right_temp);
      sampleIDs_left_temp.clear();
      sampleIDs_right_temp.clear();
      
    }
    
    nodeIDs_level = nodeIDs_level_next;
    nodeIDs_level_next.clear();
    
    // compute projected partitions at the current tree level
    for (std::map<std::vector<size_t>, std::vector<std::vector<size_t>>>::iterator it = nodeIDs_varIDs_sampleIDs.begin(); it != nodeIDs_varIDs_sampleIDs.end(); ++it){
      
      varIDs = it->second[0];
      varIDsOut = it->second[1];
      sampleIDs_temp_inb = it->second[2];
      sampleIDs_temp_oob = it->second[3];
      nodeIDs_temp.push_back({});
      bool allTerminal = true;
      
      // TO DO randomized nodeID order
      for (auto & nodeID : it->first){
        
        bool isTerminal = false;
        if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
          isTerminal = true;
        }else{
          split_varID = split_varIDs[nodeID];
        }
        allTerminal = allTerminal && isTerminal;
        
        for (auto & nodeID_current : nodeIDs_temp){
          
          if (nodeID_current.size() > 0){
            nodeIDs = nodeID_current;
            nodeIDs_left = nodeID_current;
            nodeIDs_right = nodeID_current;
            varIDs_sampleIDs_current = nodeIDs_varIDs_sampleIDs_temp[nodeID_current];
            varIDs = varIDs_sampleIDs_current[0];
            varIDsOut = varIDs_sampleIDs_current[1];
            sampleIDs_temp_inb = varIDs_sampleIDs_current[2];
            sampleIDs_temp_oob = varIDs_sampleIDs_current[3];
          }
          
          if (isTerminal){
            
            nodeIDs.push_back(nodeID);
            nodeIDs_next.push_back(nodeIDs);
            varIDs_sampleIDs.push_back(varIDs);
            varIDs_sampleIDs.push_back(varIDsOut);
            varIDs_sampleIDs.push_back(sampleIDs_temp_inb);
            varIDs_sampleIDs.push_back(sampleIDs_temp_oob);
            nodeIDs_varIDs_sampleIDs_next[nodeIDs] = varIDs_sampleIDs;
            
          } else{
            
            bool varIn = find(varIDs.begin(), varIDs.end(), split_varID) != varIDs.end(); // data has already been split using split_varID
            bool varOut = (find(varIDsOut.begin(), varIDsOut.end(), split_varID) != varIDsOut.end()) || (tree_level >= tree_depth && !varIn); // a split with split_varID has already been discarded
            bool varNew = (!varIn) && (!varOut); // split_varID has not been met before
            
            // check if new U is in UlistMC
            if (varNew){
              varIDs_check = varIDs;
              varIDs_check.push_back(split_varID);
              std::sort(varIDs_check.begin(), varIDs_check.end());
              bool inUlist = false;
              size_t u = 0;
              while (!inUlist && u < UlistMC.size()){
                U = UlistMC[u];
                inUlist = std::includes(U.begin(), U.end(), varIDs_check.begin(), varIDs_check.end());
                u += 1;
              }
              if (!inUlist){
                varNew = false;
                varOut = true;
                varIDsOut.push_back(split_varID);
              }
            }
            
            if (Ucomplement){
              bool varTemp = varIn;
              varIn = varOut;
              varOut = varTemp;
            }
            
            // varIDs
            if (varIn){
              varIDs_sampleIDs_left.push_back(varIDs);
              varIDs_sampleIDs_right.push_back(varIDs);
              varIDs_sampleIDs_left.push_back(varIDsOut);
              varIDs_sampleIDs_right.push_back(varIDsOut);
            }
            if (varOut){
              varIDs_sampleIDs.push_back(varIDs);
              varIDs_sampleIDs.push_back(varIDsOut);
            }
            if (varNew){
              varIDs_next = varIDs;
              varIDsOut_next = varIDsOut;
              if (!Ucomplement){
                varIDs_sampleIDs.push_back(varIDs_next);
                varIDs_next.push_back(split_varID);
                std::sort(varIDs_next.begin(), varIDs_next.end());
                varIDs_sampleIDs_left.push_back(varIDs_next);
                varIDs_sampleIDs_right.push_back(varIDs_next);
                varIDs_sampleIDs_left.push_back(varIDsOut_next);
                varIDs_sampleIDs_right.push_back(varIDsOut_next);
                varIDsOut_next.push_back(split_varID);
                std::sort(varIDsOut_next.begin(), varIDsOut_next.end());
                varIDs_sampleIDs.push_back(varIDsOut_next);
              }else{
                varIDs_sampleIDs_left.push_back(varIDs_next);
                varIDs_sampleIDs_right.push_back(varIDs_next);
                varIDs_next.push_back(split_varID);
                std::sort(varIDs_next.begin(), varIDs_next.end());
                varIDs_sampleIDs.push_back(varIDs_next);
                varIDs_sampleIDs.push_back(varIDsOut_next);
                varIDsOut_next.push_back(split_varID);
                std::sort(varIDsOut_next.begin(), varIDsOut_next.end());
                varIDs_sampleIDs_left.push_back(varIDsOut_next);
                varIDs_sampleIDs_right.push_back(varIDsOut_next);
              }
            }
            
            // nodeIDs & sampleIDs
            // if (!varOut){ // node split generates two child nodes
            if (varIn || varNew){
              // nodeIDs
              nodeIDs_left.push_back(child_nodeIDs[0][nodeID]);
              nodeIDs_right.push_back(child_nodeIDs[1][nodeID]);
              // sampleIDs in-bag
              set_intersection(sampleIDs_temp_inb.begin(), sampleIDs_temp_inb.end(), sampleIDs_left_inb[nodeID].begin(),
                               sampleIDs_left_inb[nodeID].end(), std::back_inserter(sampleIDs_left_temp));
              set_intersection(sampleIDs_temp_inb.begin(), sampleIDs_temp_inb.end(), sampleIDs_right_inb[nodeID].begin(),
                               sampleIDs_right_inb[nodeID].end(), std::back_inserter(sampleIDs_right_temp));
              varIDs_sampleIDs_left.push_back(sampleIDs_left_temp);
              varIDs_sampleIDs_right.push_back(sampleIDs_right_temp);
              // stop if cell is empty
              bool valid_cell_left_inb = sampleIDs_left_temp.size() > min_terminal_size;
              bool valid_cell_right_inb = sampleIDs_right_temp.size() > min_terminal_size;
              sampleIDs_left_temp.clear();
              sampleIDs_right_temp.clear();
              // sampleIDs oob-bag
              set_intersection(sampleIDs_temp_oob.begin(), sampleIDs_temp_oob.end(), sampleIDs_left_oob[nodeID].begin(),
                               sampleIDs_left_oob[nodeID].end(), std::back_inserter(sampleIDs_left_temp));
              set_intersection(sampleIDs_temp_oob.begin(), sampleIDs_temp_oob.end(), sampleIDs_right_oob[nodeID].begin(),
                               sampleIDs_right_oob[nodeID].end(), std::back_inserter(sampleIDs_right_temp));
              varIDs_sampleIDs_left.push_back(sampleIDs_left_temp);
              varIDs_sampleIDs_right.push_back(sampleIDs_right_temp);
              // stop if cell is empty
              bool valid_cell_left_oob = sampleIDs_left_temp.size() > 0;
              bool valid_cell_right_oob = sampleIDs_right_temp.size() > 0;
              sampleIDs_left_temp.clear();
              sampleIDs_right_temp.clear();
              // store new cells
              if (valid_cell_left_oob){
                if (valid_cell_left_inb){
                  nodeIDs_varIDs_sampleIDs_next[nodeIDs_left] = varIDs_sampleIDs_left;
                  nodeIDs_next.push_back(nodeIDs_left);
                } else{
                  varIDs_sampleIDs_left[0] = varIDs;
                  varIDs_sampleIDs_left[1] = varIDsOut;
                  varIDs_sampleIDs_left[2] = sampleIDs_temp_inb;
                  nodeIDs_varIDs_sampleIDs_terminal[nodeIDs_left] = varIDs_sampleIDs_left;
                }
              }
              if (valid_cell_right_oob){
                if (valid_cell_right_inb){
                  nodeIDs_varIDs_sampleIDs_next[nodeIDs_right] = varIDs_sampleIDs_right;
                  nodeIDs_next.push_back(nodeIDs_right);
                } else{
                  varIDs_sampleIDs_right[0] = varIDs;
                  varIDs_sampleIDs_right[1] = varIDsOut;
                  varIDs_sampleIDs_right[2] = sampleIDs_temp_inb;
                  nodeIDs_varIDs_sampleIDs_terminal[nodeIDs_right] = varIDs_sampleIDs_right;
                }
              }
            }
            // if (!varIn){ // split is ignored, generating a new cell with multiple children
            if (varOut || varNew){
              // nodeIDs
              nodeIDs.push_back(child_nodeIDs[0][nodeID]);
              nodeIDs.push_back(child_nodeIDs[1][nodeID]);
              nodeIDs_next.push_back(nodeIDs);
              // sampleIDs
              varIDs_sampleIDs.push_back(sampleIDs_temp_inb);
              varIDs_sampleIDs.push_back(sampleIDs_temp_oob);
              // store new cell
              nodeIDs_varIDs_sampleIDs_next[nodeIDs] = varIDs_sampleIDs;
            }
            
          }
          
          // clean
          sampleIDs_temp_inb.clear();
          sampleIDs_temp_oob.clear();
          nodeIDs_left.clear();
          nodeIDs_right.clear();
          nodeIDs.clear();
          varIDs_sampleIDs_left.clear();
          varIDs_sampleIDs_right.clear();
          varIDs_sampleIDs.clear();
          
        }
        
        nodeIDs_varIDs_sampleIDs_temp = nodeIDs_varIDs_sampleIDs_next;
        nodeIDs_varIDs_sampleIDs_next.clear();
        nodeIDs_temp = nodeIDs_next;
        nodeIDs_next.clear();
        
      }
      
      if (allTerminal){
        nodeIDs_varIDs_sampleIDs_terminal[it->first] = it->second;
      }
      
      if (!allTerminal){
        nodeIDs_varIDs_sampleIDs_level.insert(nodeIDs_varIDs_sampleIDs_temp.begin(), nodeIDs_varIDs_sampleIDs_temp.end());
      }
      nodeIDs_varIDs_sampleIDs_temp.clear();
      nodeIDs_temp.clear();

    }
    
    nodeIDs_varIDs_sampleIDs = nodeIDs_varIDs_sampleIDs_level;
    nodeIDs_varIDs_sampleIDs_level.clear();
    tree_level += 1;
    
    levels_nodeIDs_varIDs_sampleIDs.push_back(nodeIDs_varIDs_sampleIDs);
    
  }
  
  nodeIDs_varIDs_sampleIDs_terminal.insert(nodeIDs_varIDs_sampleIDs.begin(), nodeIDs_varIDs_sampleIDs.end());
  
  // nodeIDs_varIDs_sampleIDs.clear();
  // for (auto & U : UlistMC){
  //   auto it = nodeIDs_varIDs_sampleIDs_terminal.find(U);
  //   if (it != nodeIDs_varIDs_sampleIDs_terminal.end()){
  //     nodeIDs_varIDs_sampleIDs.insert(std::pair<std::vector<size_t>, std::vector<std::vector<size_t>>> (U, it->second));
  //   }
  // }
  // nodeIDs_varIDs_sampleIDs_terminal = nodeIDs_varIDs_sampleIDs;
  
  // compute predictions
  std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<double>>> predictions;
  double cell_mean;
  size_t nsample = prediction_data->getNumRows();
  std::vector<double> pred_null(nsample, 0);
  std::vector<size_t> pred_sampleIDs;
  std::pair<std::vector<size_t>, std::vector<double>> pred_sampleIDs_values;
  
  for (std::map<std::vector<size_t>, std::vector<std::vector<size_t>>>::iterator it = nodeIDs_varIDs_sampleIDs_terminal.begin(); it != nodeIDs_varIDs_sampleIDs_terminal.end(); ++it){
    
    varIDs = it->second[0];
    std::sort(varIDs.begin(), varIDs.end());
    pred_sampleIDs_values = std::pair<std::vector<size_t>, std::vector<double>> (pred_sampleIDs, pred_null);
    predictions.insert(std::pair<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<double>>> (varIDs, pred_sampleIDs_values));
    pred_sampleIDs_values = predictions[varIDs];
    pred_sampleIDs = pred_sampleIDs_values.first;
    std::vector<double> pred_temp = pred_sampleIDs_values.second;
    
    double cell_mean = 0;
    size_t cell_size = 0;
    for (auto & i : it->second[2]){
      cell_mean += prediction_data->get_y(i, 0)*sampleIDs_count[i];
      cell_size += sampleIDs_count[i];
    }
    cell_mean = cell_mean/cell_size;
    
    for (auto & i : it->second[3]){
      pred_temp[i] = cell_mean;
      pred_sampleIDs.push_back(i);
    }
    
    pred_sampleIDs_values = std::pair<std::vector<size_t>, std::vector<double>> (pred_sampleIDs, pred_temp);
    predictions[varIDs] = pred_sampleIDs_values;
    pred_sampleIDs.clear();
      
    // }
    
  }
  
  return(predictions);
  
}

std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<double>>> Tree::predictOobProjectedDepthAll(size_t depth, size_t tree_depth, const Data* prediction_data){
  
  
  // Initialize variables
  std::vector<size_t> sampleIDs_unique;
  std::map<size_t, size_t> sampleIDs_count;
  std::pair<std::map<size_t, size_t>::iterator, bool> itIDs; 
  std::sort(sampleIDs.begin(), sampleIDs.end());
  std::sort(oob_sampleIDs.begin(), oob_sampleIDs.end());
  // Deduplicate boostrapped training data
  for (auto & i : sampleIDs){
    itIDs = sampleIDs_count.insert(std::pair<size_t, size_t> (i, 1));
    if (!itIDs.second){
      itIDs.first->second += 1;
    }else{
      sampleIDs_unique.push_back(i);
    }
  }
  
  
  // Split the full dataset at each node
  std::vector<std::vector<size_t>> sampleIDs_left_inb;
  std::vector<std::vector<size_t>> sampleIDs_right_inb;
  std::vector<std::vector<size_t>> sampleIDs_left_oob;
  std::vector<std::vector<size_t>> sampleIDs_right_oob;
  std::vector<size_t> sampleIDs_left_temp;
  std::vector<size_t> sampleIDs_right_temp;

  
  // Initialize variables
  // Maps to define projected partitions: key is the nodeID sequence of the projected cell, value is 
  // a list of vectors containing the list of splitting/ignored variables and the INB/OOB samples of the cell.
  std::vector<std::map<std::vector<size_t>, std::vector<std::vector<size_t>>>> levels_nodeIDs_varIDs_sampleIDs;
  std::map<std::vector<size_t>, std::vector<std::vector<size_t>>> nodeIDs_varIDs_sampleIDs;
  std::map<std::vector<size_t>, std::vector<std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_temp;
  std::map<std::vector<size_t>, std::vector<std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_next;
  std::map<std::vector<size_t>, std::vector<std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_level;
  std::map<std::vector<size_t>, std::vector<std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_terminal;
  std::vector<std::vector<size_t>> varIDs_sampleIDs;
  std::vector<std::vector<size_t>> varIDs_sampleIDs_left;
  std::vector<std::vector<size_t>> varIDs_sampleIDs_right;
  std::vector<std::vector<size_t>> varIDs_sampleIDs_current;
  std::vector<size_t> sampleIDs_temp_inb;
  std::vector<size_t> sampleIDs_temp_oob;
  std::vector<size_t> varIDs;
  std::vector<size_t> varIDsOut;
  std::vector<size_t> varIDs_next;
  std::vector<size_t> varIDsOut_next;
  std::vector<size_t> nodeIDs;
  std::vector<size_t> nodeIDs_level;
  std::vector<size_t> nodeIDs_level_next;
  std::vector<size_t> nodeIDs_left;
  std::vector<size_t> nodeIDs_right;
  std::vector<std::vector<size_t>> nodeIDs_temp;
  std::vector<std::vector<size_t>> nodeIDs_next;
  size_t split_varID;
  

  // Initialize with first tree level
  nodeIDs.push_back(0);
  nodeIDs_level = nodeIDs;
  varIDs_sampleIDs.push_back(varIDs);
  varIDs_sampleIDs.push_back(varIDsOut);
  varIDs_sampleIDs.push_back(sampleIDs_unique);
  varIDs_sampleIDs.push_back(oob_sampleIDs);
  nodeIDs_varIDs_sampleIDs[nodeIDs] = varIDs_sampleIDs;
  nodeIDs.clear();
  varIDs_sampleIDs.clear();
  size_t tree_level = 0;
  size_t min_terminal_size = 5;
  
  
  // loop through all tree levels to compute projected partition
  while (tree_level < tree_depth){
  // while (nodeIDs_varIDs_sampleIDs.size() > 0){
    
    // Split the full dataset at each node of the current tree level 
    for (auto & nodeID : nodeIDs_level){
      
      if (child_nodeIDs[0][nodeID] != 0 && child_nodeIDs[1][nodeID] != 0) {
        nodeIDs_level_next.push_back(child_nodeIDs[0][nodeID]);
        nodeIDs_level_next.push_back(child_nodeIDs[1][nodeID]);
      }
      
      size_t split_varID = split_varIDs[nodeID];
      double split_value = split_values[nodeID];
      
      // in-bag sample
      for (auto & i : sampleIDs_unique) {
        double value = prediction_data->get_x(i, split_varID);
        if (prediction_data->isOrderedVariable(split_varID)) {
          if (value <= split_value) {
            // Move to left child
            sampleIDs_left_temp.push_back(i);
          } else {
            // Move to right child
            sampleIDs_right_temp.push_back(i);
          }
        } else {
          size_t factorID = floor(value) - 1;
          size_t splitID = floor(split_value);
          // Left if 0 found at position factorID
          if (!(splitID & (1ULL << factorID))) {
            // Move to left child
            sampleIDs_left_temp.push_back(i);
          } else {
            // Move to right child
            sampleIDs_right_temp.push_back(i);
          }
        }
      }
      sampleIDs_left_inb.push_back(sampleIDs_left_temp);
      sampleIDs_right_inb.push_back(sampleIDs_right_temp);
      sampleIDs_left_temp.clear();
      sampleIDs_right_temp.clear();
      
      // oob-bag sample
      for (auto & i : oob_sampleIDs) {
        double value = prediction_data->get_x(i, split_varID);
        if (prediction_data->isOrderedVariable(split_varID)) {
          if (value <= split_value) {
            // Move to left child
            sampleIDs_left_temp.push_back(i);
          } else {
            // Move to right child
            sampleIDs_right_temp.push_back(i);
          }
        } else {
          size_t factorID = floor(value) - 1;
          size_t splitID = floor(split_value);
          // Left if 0 found at position factorID
          if (!(splitID & (1ULL << factorID))) {
            // Move to left child
            sampleIDs_left_temp.push_back(i);
          } else {
            // Move to right child
            sampleIDs_right_temp.push_back(i);
          }
        }
      }
      sampleIDs_left_oob.push_back(sampleIDs_left_temp);
      sampleIDs_right_oob.push_back(sampleIDs_right_temp);
      sampleIDs_left_temp.clear();
      sampleIDs_right_temp.clear();
      
    }
    
    nodeIDs_level = nodeIDs_level_next;
    nodeIDs_level_next.clear();
    
    // compute projected partitions at the current tree level
    for (std::map<std::vector<size_t>, std::vector<std::vector<size_t>>>::iterator it = nodeIDs_varIDs_sampleIDs.begin(); it != nodeIDs_varIDs_sampleIDs.end(); ++it){

      varIDs = it->second[0];
      varIDsOut = it->second[1];
      sampleIDs_temp_inb = it->second[2];
      sampleIDs_temp_oob = it->second[3];
      nodeIDs_temp.push_back({});
      bool allTerminal = true;
      
      // TO DO randomized nodeID order
      for (auto & nodeID : it->first){
        
        bool isTerminal = false;
        if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
          isTerminal = true;
        }else{
          split_varID = split_varIDs[nodeID];
        }
        allTerminal = allTerminal && isTerminal;

        for (auto & nodeID_current : nodeIDs_temp){

          if (nodeID_current.size() > 0){
            nodeIDs = nodeID_current;
            nodeIDs_left = nodeID_current;
            nodeIDs_right = nodeID_current;
            varIDs_sampleIDs_current = nodeIDs_varIDs_sampleIDs_temp[nodeID_current];
            varIDs = varIDs_sampleIDs_current[0];
            varIDsOut = varIDs_sampleIDs_current[1];
            sampleIDs_temp_inb = varIDs_sampleIDs_current[2];
            sampleIDs_temp_oob = varIDs_sampleIDs_current[3];
          }
          
          if (isTerminal){
            
            nodeIDs.push_back(nodeID);
            nodeIDs_next.push_back(nodeIDs);
            varIDs_sampleIDs.push_back(varIDs);
            varIDs_sampleIDs.push_back(varIDsOut);
            varIDs_sampleIDs.push_back(sampleIDs_temp_inb);
            varIDs_sampleIDs.push_back(sampleIDs_temp_oob);
            nodeIDs_varIDs_sampleIDs_next[nodeIDs] = varIDs_sampleIDs;
            
          } else{
          
            bool varIn = find(varIDs.begin(), varIDs.end(), split_varID) != varIDs.end(); // data has already been split using split_varID
            bool varOut = (find(varIDsOut.begin(), varIDsOut.end(), split_varID) != varIDsOut.end()) || (tree_level >= depth && !varIn); // a split with split_varID has already been discarded
            bool varNew = (!varIn) && (!varOut) && tree_level < depth; // split_varID has not been met before
    
            // varIDs
            if (varIn){
              varIDs_sampleIDs_left.push_back(varIDs);
              varIDs_sampleIDs_right.push_back(varIDs);
              varIDs_sampleIDs_left.push_back(varIDsOut);
              varIDs_sampleIDs_right.push_back(varIDsOut);
            }
            if (varOut){
              varIDs_sampleIDs.push_back(varIDs);
              varIDs_sampleIDs.push_back(varIDsOut);
            }
            if (varNew){
              varIDs_next = varIDs;
              varIDsOut_next = varIDsOut;
              varIDs_sampleIDs.push_back(varIDs_next);
              varIDs_next.push_back(split_varID);
              varIDs_sampleIDs_left.push_back(varIDs_next);
              varIDs_sampleIDs_right.push_back(varIDs_next);
              varIDs_sampleIDs_left.push_back(varIDsOut_next);
              varIDs_sampleIDs_right.push_back(varIDsOut_next);
              varIDsOut_next.push_back(split_varID);
              varIDs_sampleIDs.push_back(varIDsOut_next);
            }
    
            // nodeIDs & sampleIDs
            // if (!varOut){ // node split generates two child nodes
            if (varIn || varNew){
              // nodeIDs
              nodeIDs_left.push_back(child_nodeIDs[0][nodeID]);
              nodeIDs_right.push_back(child_nodeIDs[1][nodeID]);
              // sampleIDs in-bag
              set_intersection(sampleIDs_temp_inb.begin(), sampleIDs_temp_inb.end(), sampleIDs_left_inb[nodeID].begin(),
                               sampleIDs_left_inb[nodeID].end(), std::back_inserter(sampleIDs_left_temp));
              set_intersection(sampleIDs_temp_inb.begin(), sampleIDs_temp_inb.end(), sampleIDs_right_inb[nodeID].begin(),
                               sampleIDs_right_inb[nodeID].end(), std::back_inserter(sampleIDs_right_temp));
              varIDs_sampleIDs_left.push_back(sampleIDs_left_temp);
              varIDs_sampleIDs_right.push_back(sampleIDs_right_temp);
              // stop if cell is empty
              bool valid_cell_left_inb = sampleIDs_left_temp.size() > min_terminal_size;
              bool valid_cell_right_inb = sampleIDs_right_temp.size() > min_terminal_size;
              sampleIDs_left_temp.clear();
              sampleIDs_right_temp.clear();
              // sampleIDs oob-bag
              set_intersection(sampleIDs_temp_oob.begin(), sampleIDs_temp_oob.end(), sampleIDs_left_oob[nodeID].begin(),
                               sampleIDs_left_oob[nodeID].end(), std::back_inserter(sampleIDs_left_temp));
              set_intersection(sampleIDs_temp_oob.begin(), sampleIDs_temp_oob.end(), sampleIDs_right_oob[nodeID].begin(),
                               sampleIDs_right_oob[nodeID].end(), std::back_inserter(sampleIDs_right_temp));
              varIDs_sampleIDs_left.push_back(sampleIDs_left_temp);
              varIDs_sampleIDs_right.push_back(sampleIDs_right_temp);
              // stop if cell is empty
              bool valid_cell_left_oob = sampleIDs_left_temp.size() > 0;
              bool valid_cell_right_oob = sampleIDs_right_temp.size() > 0;
              sampleIDs_left_temp.clear();
              sampleIDs_right_temp.clear();
              // store new cells
              if (valid_cell_left_oob){
                if (valid_cell_left_inb){
                  nodeIDs_varIDs_sampleIDs_next[nodeIDs_left] = varIDs_sampleIDs_left;
                  nodeIDs_next.push_back(nodeIDs_left);
                } else{
                  varIDs_sampleIDs_left[0] = varIDs;
                  varIDs_sampleIDs_left[1] = varIDsOut;
                  varIDs_sampleIDs_left[2] = sampleIDs_temp_inb;
                  nodeIDs_varIDs_sampleIDs_terminal[nodeIDs_left] = varIDs_sampleIDs_left;
                }
              }
              if (valid_cell_right_oob){
                if (valid_cell_right_inb){
                  nodeIDs_varIDs_sampleIDs_next[nodeIDs_right] = varIDs_sampleIDs_right;
                  nodeIDs_next.push_back(nodeIDs_right);
                } else{
                  varIDs_sampleIDs_right[0] = varIDs;
                  varIDs_sampleIDs_right[1] = varIDsOut;
                  varIDs_sampleIDs_right[2] = sampleIDs_temp_inb;
                  nodeIDs_varIDs_sampleIDs_terminal[nodeIDs_right] = varIDs_sampleIDs_right;
                }
              }
            }
            // if (!varIn){ // split is ignored, generating a new cell with multiple children
            if (varOut || varNew){
              // nodeIDs
              nodeIDs.push_back(child_nodeIDs[0][nodeID]);
              nodeIDs.push_back(child_nodeIDs[1][nodeID]);
              nodeIDs_next.push_back(nodeIDs);
              // sampleIDs
              varIDs_sampleIDs.push_back(sampleIDs_temp_inb);
              varIDs_sampleIDs.push_back(sampleIDs_temp_oob);
              // store new cell
              nodeIDs_varIDs_sampleIDs_next[nodeIDs] = varIDs_sampleIDs;
            }
            
          }
          
          // clean
          sampleIDs_temp_inb.clear();
          sampleIDs_temp_oob.clear();
          nodeIDs_left.clear();
          nodeIDs_right.clear();
          nodeIDs.clear();
          varIDs_sampleIDs_left.clear();
          varIDs_sampleIDs_right.clear();
          varIDs_sampleIDs.clear();
          
        }
        
        nodeIDs_varIDs_sampleIDs_temp = nodeIDs_varIDs_sampleIDs_next;
        nodeIDs_varIDs_sampleIDs_next.clear();
        nodeIDs_temp = nodeIDs_next;
        nodeIDs_next.clear();

      }
      
      if (allTerminal){
        nodeIDs_varIDs_sampleIDs_terminal[it->first] = it->second;
      }
      
      if (!allTerminal){
        nodeIDs_varIDs_sampleIDs_level.insert(nodeIDs_varIDs_sampleIDs_temp.begin(), nodeIDs_varIDs_sampleIDs_temp.end());
      }
      nodeIDs_varIDs_sampleIDs_temp.clear();
      nodeIDs_temp.clear();
      
      // filter empty cells
      // TO DO: stop computations during nodeIDs_varIDs_sampleIDs_temp filling
      // for (std::map<std::vector<size_t>, std::vector<std::vector<size_t>>>::iterator it_temp = nodeIDs_varIDs_sampleIDs_temp.begin();
      //      it_temp != nodeIDs_varIDs_sampleIDs_temp.end(); ++it_temp){
      //   if (it_temp->second[2].size() > 0 && it_temp->second[3].size() > 0){
      //     nodeIDs_varIDs_sampleIDs_level.insert(std::pair<std::vector<size_t>, std::vector<std::vector<size_t>>> (it_temp->first, it_temp->second));
      //   }
      //   if (it_temp->second[2].size() == 0 && it_temp->second[3].size() > 0){
      //     it->second[3] = it_temp->second[3];
      //     nodeIDs_varIDs_sampleIDs_terminal.insert(std::pair<std::vector<size_t>, std::vector<std::vector<size_t>>> (it->first, it->second));
      //   }
      // }

    }

    nodeIDs_varIDs_sampleIDs = nodeIDs_varIDs_sampleIDs_level;
    nodeIDs_varIDs_sampleIDs_level.clear();
    tree_level += 1;
    
    levels_nodeIDs_varIDs_sampleIDs.push_back(nodeIDs_varIDs_sampleIDs);

  }
  
  nodeIDs_varIDs_sampleIDs_terminal.insert(nodeIDs_varIDs_sampleIDs.begin(), nodeIDs_varIDs_sampleIDs.end());
  
  
  // compute predictions
  std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<double>>> predictions;
  double cell_mean;
  size_t nsample = prediction_data->getNumRows();
  std::vector<double> pred_null(nsample, 0);
  std::vector<size_t> pred_sampleIDs;
  std::pair<std::vector<size_t>, std::vector<double>> pred_sampleIDs_values;
  
  for (std::map<std::vector<size_t>, std::vector<std::vector<size_t>>>::iterator it = nodeIDs_varIDs_sampleIDs_terminal.begin(); it != nodeIDs_varIDs_sampleIDs_terminal.end(); ++it){
   
    varIDs = it->second[0];
    std::sort(varIDs.begin(), varIDs.end());
    pred_sampleIDs_values = std::pair<std::vector<size_t>, std::vector<double>> (pred_sampleIDs, pred_null);
    predictions.insert(std::pair<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<double>>> (varIDs, pred_sampleIDs_values));
    pred_sampleIDs_values = predictions[varIDs];
    pred_sampleIDs = pred_sampleIDs_values.first;
    std::vector<double> pred_temp = pred_sampleIDs_values.second;
    
    double cell_mean = 0;
    size_t cell_size = 0;
    for (auto & i : it->second[2]){
      cell_mean += prediction_data->get_y(i, 0)*sampleIDs_count[i];
      cell_size += sampleIDs_count[i];
    }
    cell_mean = cell_mean/cell_size;
    
    for (auto & i : it->second[3]){
      pred_temp[i] = cell_mean;
      pred_sampleIDs.push_back(i);
    }
    
    pred_sampleIDs_values = std::pair<std::vector<size_t>, std::vector<double>> (pred_sampleIDs, pred_temp);
    predictions[varIDs] = pred_sampleIDs_values;
    pred_sampleIDs.clear();
    
  }
  
  return(predictions);
  
  // debug output
  // std::vector<std::vector<std::vector<double>>> res;
  // std::vector<std::vector<double>> res_temp;
  // for (std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<double>>>::iterator it = predictions.begin(); it != predictions.end(); ++it){
  //   std::vector<double> varU(it->first.begin(), it->first.end());
  //   res_temp.push_back(varU);
  //   std::vector<double> oobIDs(it->second.first.begin(), it->second.first.end());
  //   res_temp.push_back(oobIDs);
  //   res_temp.push_back(it->second.second);
  //   res.push_back(res_temp);
  //   res_temp.clear();
  // }
  //   
  // return(res);

  // debug for output
  // std::vector<std::vector<std::vector<std::vector<size_t>>>> res_levels;
  // std::vector<std::vector<std::vector<size_t>>> res;
  // std::vector<std::vector<size_t>> res_nodeIDs;
  // for (auto & map_varIDs : levels_nodeIDs_varIDs_sampleIDs){
  //   // for (std::map<std::vector<size_t>, std::vector<std::vector<size_t>>>::iterator it = nodeIDs_varIDs_sampleIDs.begin(); it != nodeIDs_varIDs_sampleIDs.end(); ++it){
  //   for (std::map<std::vector<size_t>, std::vector<std::vector<size_t>>>::iterator it = map_varIDs.begin(); it != map_varIDs.end(); ++it){
  //     res_nodeIDs.push_back(it->first);
  //     res_nodeIDs.push_back(it->second[0]);
  //     res_nodeIDs.push_back(it->second[1]);
  //     res_nodeIDs.push_back(it->second[2]);
  //     res_nodeIDs.push_back(it->second[3]);
  //     res.push_back(res_nodeIDs);
  //     res_nodeIDs.clear();
  //   }
  //   res_levels.push_back(res);
  //   res.clear();
  // }
  // 
  // return(res_levels);
  
}

std::vector<std::vector<std::vector<size_t>>> Tree::predictOobProjectedDepthAllPath(size_t depth, const Data* prediction_data){
  
  // Initialize variables
  std::vector<size_t> sampleIDs_unique;
  std::map<size_t, size_t> sampleIDs_count;
  std::pair<std::map<size_t, size_t>::iterator, bool> itIDs; 
  std::sort(sampleIDs.begin(), sampleIDs.end());
  std::sort(oob_sampleIDs.begin(), oob_sampleIDs.end());
  for (auto & i : sampleIDs){
    itIDs = sampleIDs_count.insert(std::pair<size_t, size_t> (i, 1));
    if (!itIDs.second){
      itIDs.first->second += 1;
    }else{
      sampleIDs_unique.push_back(i);
    }
  }

  // Split the full dataset at each node
  std::vector<std::vector<size_t>> sampleIDs_left_inb;
  std::vector<std::vector<size_t>> sampleIDs_right_inb;
  std::vector<std::vector<size_t>> sampleIDs_left_oob;
  std::vector<std::vector<size_t>> sampleIDs_right_oob;
  std::vector<size_t> sampleIDs_left_temp;
  std::vector<size_t> sampleIDs_right_temp;
  size_t num_nodes = pow(2, depth) - 1;
  
  for (size_t nodeID = 0; nodeID < num_nodes; ++nodeID){
    
    size_t split_varID = split_varIDs[nodeID];
    double split_value = split_values[nodeID];
    
    // in-bag sample
    for (auto & i : sampleIDs_unique) {
      double value = prediction_data->get_x(i, split_varID);
      if (prediction_data->isOrderedVariable(split_varID)) {
        if (value <= split_value) {
          // Move to left child
          sampleIDs_left_temp.push_back(i);
        } else {
          // Move to right child
          sampleIDs_right_temp.push_back(i);
        }
      } else {
        size_t factorID = floor(value) - 1;
        size_t splitID = floor(split_value);
        // Left if 0 found at position factorID
        if (!(splitID & (1ULL << factorID))) {
          // Move to left child
          sampleIDs_left_temp.push_back(i);
        } else {
          // Move to right child
          sampleIDs_right_temp.push_back(i);
        }
      }
    }
    sampleIDs_left_inb.push_back(sampleIDs_left_temp);
    sampleIDs_right_inb.push_back(sampleIDs_right_temp);
    sampleIDs_left_temp.clear();
    sampleIDs_right_temp.clear();
    
    // oob-bag sample
    for (auto & i : oob_sampleIDs) {
      double value = prediction_data->get_x(i, split_varID);
      if (prediction_data->isOrderedVariable(split_varID)) {
        if (value <= split_value) {
          // Move to left child
          sampleIDs_left_temp.push_back(i);
        } else {
          // Move to right child
          sampleIDs_right_temp.push_back(i);
        }
      } else {
        size_t factorID = floor(value) - 1;
        size_t splitID = floor(split_value);
        // Left if 0 found at position factorID
        if (!(splitID & (1ULL << factorID))) {
          // Move to left child
          sampleIDs_left_temp.push_back(i);
        } else {
          // Move to right child
          sampleIDs_right_temp.push_back(i);
        }
      }
    }
    sampleIDs_left_oob.push_back(sampleIDs_left_temp);
    sampleIDs_right_oob.push_back(sampleIDs_right_temp);
    sampleIDs_left_temp.clear();
    sampleIDs_right_temp.clear();

  }
  
  // Initialize variables
  std::vector<std::map<std::vector<size_t>, std::vector<size_t>>> nodeIDs_varIDs_sampleIDs;
  std::vector<std::map<std::vector<size_t>, std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_next;
  std::map<std::vector<size_t>, std::vector<size_t>> varIDs_sampleIDs;
  std::map<std::vector<size_t>, std::vector<size_t>> varIDs_sampleIDs_left;
  std::map<std::vector<size_t>, std::vector<size_t>> varIDs_sampleIDs_right;
  std::vector<std::map<std::vector<size_t>, std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_oob;
  std::vector<std::map<std::vector<size_t>, std::vector<size_t>>> nodeIDs_varIDs_sampleIDs_next_oob;
  std::map<std::vector<size_t>, std::vector<size_t>> varIDs_sampleIDs_oob;
  std::map<std::vector<size_t>, std::vector<size_t>> varIDs_sampleIDs_left_oob;
  std::map<std::vector<size_t>, std::vector<size_t>> varIDs_sampleIDs_right_oob;
  std::vector<std::vector<size_t>> varIDs_all;
  std::vector<size_t> nodeID_varIDs;
  std::vector<size_t> varIDs;
  std::vector<std::vector<size_t>> varIDs_all_next;
  size_t split_varID;
  
  // Initialize with first tree level
  size_t nodeID = 0;
  varIDs.push_back(split_varIDs[nodeID]);
  // left child
  varIDs_sampleIDs[varIDs] = sampleIDs_left_inb[nodeID];
  nodeIDs_varIDs_sampleIDs.push_back(varIDs_sampleIDs);
  varIDs_sampleIDs.clear();
  varIDs_sampleIDs_oob[varIDs] = sampleIDs_left_oob[nodeID];
  nodeIDs_varIDs_sampleIDs_oob.push_back(varIDs_sampleIDs_oob);
  varIDs_sampleIDs_oob.clear();
  varIDs_all.push_back(varIDs);
  // right child
  varIDs_sampleIDs[varIDs] = sampleIDs_right_inb[nodeID];
  nodeIDs_varIDs_sampleIDs.push_back(varIDs_sampleIDs);
  varIDs_sampleIDs.clear();
  varIDs_sampleIDs_oob[varIDs] = sampleIDs_right_oob[nodeID];
  nodeIDs_varIDs_sampleIDs_oob.push_back(varIDs_sampleIDs_oob);
  varIDs_sampleIDs_oob.clear();
  varIDs_all.push_back(varIDs);
  varIDs.clear();
  nodeID += 1;
  size_t tree_level = 1;

  while (tree_level < depth){
    
    for (size_t k = 0; k < nodeIDs_varIDs_sampleIDs.size(); ++k){
      
      varIDs_sampleIDs = nodeIDs_varIDs_sampleIDs[k];
      varIDs_sampleIDs_left = varIDs_sampleIDs;
      varIDs_sampleIDs_right = varIDs_sampleIDs;
      varIDs_sampleIDs_oob = nodeIDs_varIDs_sampleIDs_oob[k];
      varIDs_sampleIDs_left_oob = varIDs_sampleIDs_oob;
      varIDs_sampleIDs_right_oob = varIDs_sampleIDs_oob;
      split_varID = split_varIDs[nodeID];
      nodeID_varIDs = varIDs_all[k];
      
      if (find(nodeID_varIDs.begin(), nodeID_varIDs.end(), split_varID) == nodeID_varIDs.end()){
        
        nodeID_varIDs.push_back(split_varID);

        for (std::map<std::vector<size_t>, std::vector<size_t>>::iterator it = varIDs_sampleIDs.begin(); it != varIDs_sampleIDs.end(); ++it){
          
          varIDs = it -> first;
          varIDs.push_back(split_varID);
          // inb
          set_intersection(it->second.begin(), it->second.end(), sampleIDs_left_inb[nodeID].begin(),
                           sampleIDs_left_inb[nodeID].end(), std::back_inserter(sampleIDs_left_temp));
          set_intersection(it->second.begin(), it->second.end(), sampleIDs_right_inb[nodeID].begin(),
                           sampleIDs_right_inb[nodeID].end(), std::back_inserter(sampleIDs_right_temp));
          varIDs_sampleIDs_left.insert(std::pair<std::vector<size_t>, std::vector<size_t>> (varIDs, sampleIDs_left_temp));
          varIDs_sampleIDs_right.insert(std::pair<std::vector<size_t>, std::vector<size_t>> (varIDs, sampleIDs_right_temp));
          sampleIDs_left_temp.clear();
          sampleIDs_right_temp.clear();
          // oob
          set_intersection(it->second.begin(), it->second.end(), sampleIDs_left_oob[nodeID].begin(),
                           sampleIDs_left_oob[nodeID].end(), std::back_inserter(sampleIDs_left_temp));
          set_intersection(it->second.begin(), it->second.end(), sampleIDs_right_oob[nodeID].begin(),
                           sampleIDs_right_oob[nodeID].end(), std::back_inserter(sampleIDs_right_temp));
          varIDs_sampleIDs_left_oob.insert(std::pair<std::vector<size_t>, std::vector<size_t>> (varIDs, sampleIDs_left_temp));
          varIDs_sampleIDs_right_oob.insert(std::pair<std::vector<size_t>, std::vector<size_t>> (varIDs, sampleIDs_right_temp));
          sampleIDs_left_temp.clear();
          sampleIDs_right_temp.clear();

        }
        
      } else {
        
        for (std::map<std::vector<size_t>, std::vector<size_t>>::iterator it = varIDs_sampleIDs.begin(); it != varIDs_sampleIDs.end(); ++it){
          
          varIDs = it -> first;
          
          if (find(varIDs.begin(), varIDs.end(), split_varID) != varIDs.end()){
            // inb
            set_intersection(it->second.begin(), it->second.end(), sampleIDs_left_inb[nodeID].begin(),
                             sampleIDs_left_inb[nodeID].end(), std::back_inserter(sampleIDs_left_temp));
            set_intersection(it->second.begin(), it->second.end(), sampleIDs_right_inb[nodeID].begin(),
                             sampleIDs_right_inb[nodeID].end(), std::back_inserter(sampleIDs_right_temp));
            varIDs_sampleIDs_left[varIDs] = sampleIDs_left_temp;
            varIDs_sampleIDs_right[varIDs] = sampleIDs_right_temp;
            sampleIDs_left_temp.clear();
            sampleIDs_right_temp.clear();
            // oob
            set_intersection(it->second.begin(), it->second.end(), sampleIDs_left_oob[nodeID].begin(),
                             sampleIDs_left_oob[nodeID].end(), std::back_inserter(sampleIDs_left_temp));
            set_intersection(it->second.begin(), it->second.end(), sampleIDs_right_oob[nodeID].begin(),
                             sampleIDs_right_oob[nodeID].end(), std::back_inserter(sampleIDs_right_temp));
            varIDs_sampleIDs_left_oob[varIDs] = sampleIDs_left_temp;
            varIDs_sampleIDs_right_oob[varIDs] = sampleIDs_right_temp;
            sampleIDs_left_temp.clear();
            sampleIDs_right_temp.clear();
          }
          
        }
        
      }
      
      varIDs_all_next.push_back(nodeID_varIDs);
      varIDs_all_next.push_back(nodeID_varIDs);
      nodeIDs_varIDs_sampleIDs_next.push_back(varIDs_sampleIDs_left);
      nodeIDs_varIDs_sampleIDs_next.push_back(varIDs_sampleIDs_right);
      nodeIDs_varIDs_sampleIDs_next_oob.push_back(varIDs_sampleIDs_left_oob);
      nodeIDs_varIDs_sampleIDs_next_oob.push_back(varIDs_sampleIDs_right_oob);
      nodeID += 1;
      
    }
    
    varIDs_all = varIDs_all_next;
    varIDs_all_next.clear();
    nodeIDs_varIDs_sampleIDs = nodeIDs_varIDs_sampleIDs_next;
    nodeIDs_varIDs_sampleIDs_next.clear();
    nodeIDs_varIDs_sampleIDs_oob = nodeIDs_varIDs_sampleIDs_next_oob;
    nodeIDs_varIDs_sampleIDs_next_oob.clear();
    tree_level += 1;
    
  }
  
  
  
  
  // debug for output
  std::vector<std::vector<std::vector<size_t>>> res;
  for (auto & varIDs_sampleIDs : nodeIDs_varIDs_sampleIDs){
    std::vector<std::vector<size_t>> res_nodeID;
    for (std::map<std::vector<size_t>, std::vector<size_t>>::iterator it = varIDs_sampleIDs.begin(); it != varIDs_sampleIDs.end(); ++it){
      // std::vector<<std::vector<size_t>> res_varIDs;
      // res_varIDs.push_back(it -> first);
      // res_varIDs.push_back(it -> second);
      // res_nodeID.push_back(res_varIDs);
      res_nodeID.push_back(it -> first);
    }
    res.push_back(res_nodeID);
  }
  
  return(res);
  
}

std::map<std::vector<size_t>, std::vector<double>> Tree::predictOobProjectedDepth(size_t depth, const Data* prediction_data) {
  
  std::vector<std::vector<size_t>> setVarU;
  setVarU = getVarU(depth);
  std::map<std::vector<size_t>, std::vector<double>> varUpredictions;
  std::vector<double> predictions;
  
  for (auto & varU : setVarU){
    predictions = predictOobProjectedVarU(varU, prediction_data);
    varUpredictions[varU] = predictions;
  }
  
  return(varUpredictions);
  
}

std::pair<std::map<std::vector<size_t>, double>, double> Tree::getSirusVarU(size_t shapley_depth) {
  
  std::map<std::vector<size_t>, double> treeU;
  size_t num_nodes = child_nodeIDs[0].size();
  std::vector<std::vector<size_t>> nodeIDs_varIDs;
  std::vector<std::vector<size_t>> nodeIDs_varIDs_next;
  std::vector<std::vector<size_t>> varIDs_terminal;
  std::vector<size_t> nodeIDs_level;
  std::vector<size_t> nodeIDs_level_next;
  
  // Initialize with tree root
  nodeIDs_level.push_back(0);
  std::vector<size_t> varIDs = {split_varIDs[0]};
  nodeIDs_varIDs.push_back(varIDs);
  // treeU.insert(std::pair<std::vector<size_t>, double> (varIDs, 1));
  size_t current_depth = 1;
  double num_paths = 0;
  
  while((nodeIDs_level.size() > 0) && (current_depth < shapley_depth)){
    for (size_t k = 0; k < nodeIDs_level.size(); ++k){
      size_t nodeID =  nodeIDs_level[k];
      if (!(child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0)) {
        for (size_t s = 0; s <= 1; ++s){
          varIDs = nodeIDs_varIDs[k];
          size_t nodeID_child = child_nodeIDs[s][nodeID];
          size_t splitID = split_varIDs[nodeID_child];
          if (find(varIDs.begin(), varIDs.end(), splitID) == varIDs.end()){
            varIDs.push_back(splitID);
            // std::sort(varIDs.begin(), varIDs.end());
            // auto it_bool = treeU.insert(std::pair<std::vector<size_t>, double> (varIDs, 1));
          }
          nodeIDs_varIDs_next.push_back(varIDs);
          nodeIDs_level_next.push_back(nodeID_child);
        }
      } else{
        num_paths += 1;
        varIDs_terminal.push_back(nodeIDs_varIDs[k]);
      }
    }
    nodeIDs_varIDs = nodeIDs_varIDs_next;
    nodeIDs_level = nodeIDs_level_next;
    nodeIDs_varIDs_next.clear();
    nodeIDs_level_next.clear();
    current_depth += 1;
  }
  num_paths += nodeIDs_level.size();
  varIDs_terminal.insert(varIDs_terminal.begin(), nodeIDs_varIDs.begin(), nodeIDs_varIDs.end());
  
  std::vector<size_t> varIDs_sort;
  for (auto & varIDs : varIDs_terminal){
    while (varIDs.size() > 0){
      varIDs_sort = varIDs;
      std::sort(varIDs_sort.begin(), varIDs_sort.end());
      auto it_bool = treeU.insert(std::pair<std::vector<size_t>, double> (varIDs_sort, 1));
      if (!it_bool.second){
        it_bool.first->second += 1;
      }
      varIDs.pop_back();
    }
  }
  
  std::pair<std::map<std::vector<size_t>, double>, double> res (treeU, num_paths);
  
  return(res);
  
}

std::pair<std::map<std::vector<size_t>, double>, double> Tree::getAllVarU(size_t shapley_depth) {
  
  std::map<std::vector<size_t>, double> treeU;
  size_t num_nodes = child_nodeIDs[0].size();
  std::vector<std::vector<size_t>> nodeIDs_varIDs(num_nodes);
  std::vector<size_t> nodeIDs_depth(num_nodes);
  nodeIDs_depth[0] = 0;
  nodeIDs_varIDs[0] = {split_varIDs[0]};
  std::vector<size_t> varIDs;
  std::vector<std::vector<std::vector<size_t>>> nodeIDs_allU(num_nodes);
  nodeIDs_allU[0].push_back(nodeIDs_varIDs[0]);
  std::vector<std::vector<size_t>> allU;
  std::vector<size_t> Utemp;
  size_t current_depth;
  double num_paths = 0;
  
  for (size_t nodeID = 0; nodeID < num_nodes; ++nodeID){
    
    varIDs = nodeIDs_varIDs[nodeID];
    allU = nodeIDs_allU[nodeID];
    current_depth = nodeIDs_depth[nodeID];
    
    if (!(child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) && (current_depth < shapley_depth)) {
      for (size_t s = 0; s <= 1; ++s){
        size_t nodeID_child = child_nodeIDs[s][nodeID];
        size_t splitID = split_varIDs[nodeID_child];
        nodeIDs_allU[nodeID_child] = allU;
        nodeIDs_depth[nodeID_child] = current_depth + 1;
        if (find(varIDs.begin(), varIDs.end(), splitID) == varIDs.end()){
          varIDs.push_back(splitID);
          nodeIDs_allU[nodeID_child].push_back({splitID});
          for (auto & U : allU){
            Utemp = U;
            Utemp.push_back(splitID);
            std::sort(Utemp.begin(), Utemp.end());
            nodeIDs_allU[nodeID_child].push_back(Utemp);
          }
        }
        nodeIDs_varIDs[nodeID_child] = varIDs;
      }
    } else{
      nodeIDs_depth[child_nodeIDs[0][nodeID]] = current_depth + 1;
      nodeIDs_depth[child_nodeIDs[1][nodeID]] = current_depth + 1;
      if (allU.size() > 0){
        num_paths += 1;
        for (auto & U : allU){
          auto it_bool = treeU.insert(std::pair<std::vector<size_t>, double> (U, 1));
          if (!it_bool.second){
            it_bool.first->second += 1;
          }
        }
      }
    }

  }
  
  std::pair<std::map<std::vector<size_t>, double>, double> res (treeU, num_paths);
  
  return(res);
   
}

std::vector<std::vector<size_t>> Tree::getVarU(size_t shapley_depth) {
  
  // get list of variables in the first depth levels of the tree
  std::vector<size_t> variableSet = {split_varIDs[0]};
  std::vector<size_t> nodeIDlevel = {0};
  std::vector<size_t> nodeIDnext;
  size_t level = 1; 
  while (level < shapley_depth){
    for (auto & nodeID : nodeIDlevel){
      variableSet.push_back(split_varIDs[child_nodeIDs[0][nodeID]]);
      variableSet.push_back(split_varIDs[child_nodeIDs[1][nodeID]]);
      nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
      nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
    }
    nodeIDlevel = nodeIDnext;
    nodeIDnext.clear();
    level += 1;
  }
  std::sort(variableSet.begin(), variableSet.end());
  variableSet.erase(unique(variableSet.begin(), variableSet.end()), variableSet.end());
  
  // get all variable combinations
  std::vector<std::vector<size_t>> setVarU;
  for (auto & j : variableSet){
    setVarU.push_back({j});
  }
  std::vector<std::vector<size_t>> setVarUtemp;
  std::vector<std::vector<size_t>> setVarUnext = setVarU;
  std::vector<size_t> Utemp;
  size_t num_variables = variableSet.size();
  size_t s = 2; // s = card(U)
  while (s <= num_variables){
    s += 1;
    for (auto & U : setVarUnext){
      for (auto & j : variableSet){
        if (std::find(U.begin(), U.end(), j) == U.end()){
          Utemp = U;
          Utemp.push_back(j);
          std::sort(Utemp.begin(), Utemp.end());
          if (std::find(setVarUtemp.begin(), setVarUtemp.end(), Utemp) == setVarUtemp.end()){
            setVarUtemp.push_back(Utemp);
          }
        }
      }
    }
    setVarU.insert(setVarU.end(), setVarUtemp.begin(), setVarUtemp.end());
    setVarUnext = setVarUtemp;
    setVarUtemp.clear();
  }
  
  return(setVarU);
 
}

std::vector<double> Tree::predictOobProjectedVarU(std::vector<size_t> varU, const Data* prediction_data) {
  
  // Initialize variables
  std::map<std::vector<size_t>, std::vector<size_t>> nodeIDs_samplesIDs_inb;
  std::map<std::vector<size_t>, std::vector<size_t>> nodeIDs_samplesIDs_oob;
  std::map<std::vector<size_t>, std::vector<size_t>>::iterator it;
  std::pair<std::map<std::vector<size_t>, std::vector<size_t>>::iterator, bool> it_bool;
  std::vector<size_t> sampleIDs_unique;
  std::map<size_t, size_t> sampleIDs_count;
  std::pair<std::map<size_t, size_t>::iterator, bool> itIDs; 
  for (auto & i : sampleIDs){
    itIDs = sampleIDs_count.insert(std::pair<size_t, size_t> (i, 1));
    if (!itIDs.second){
      itIDs.first->second += 1;
    }else{
      sampleIDs_unique.push_back(i);
    }
  }
  std::vector<size_t> nodeIDroot = {0};
  std::vector<size_t> nodeIDtemp;
  std::vector<size_t> nodeIDnext;
  std::vector<size_t> nodeIDpred;
  std::vector<size_t> nodeIDlevel;
  std::vector<size_t> nodeIDpredtemp;
  
  // For each in-bag sample start in root, drop down the tree and return node set at each level
  for (auto & i : sampleIDs_unique) {
    
    nodeIDtemp = nodeIDroot;
    nodeIDnext = {};
    nodeIDpred = {};
    
    while (nodeIDtemp.size() > 0) {
      
      for (auto & nodeID : nodeIDtemp){
        if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
          nodeIDpred.push_back(nodeID);
        }else{
          size_t split_varID = split_varIDs[nodeID];
          // tree path depends whether splitting variable is in varU or not
          if (find(varU.begin(), varU.end(), split_varID) != varU.end()){
            // Move to child if splitting variable is in varU
            double value = prediction_data->get_x(i, split_varID);
            if (prediction_data->isOrderedVariable(split_varID)) {
              if (value <= split_values[nodeID]) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            } else {
              size_t factorID = floor(value) - 1;
              size_t splitID = floor(split_values[nodeID]);
              // Left if 0 found at position factorID
              if (!(splitID & (1ULL << factorID))) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            }
          }else{
            // When splitting on variable not in varU, move to both left and right children
            nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
            nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
          }
        }
      }
      
      if (nodeIDnext.size() > 0 || nodeIDtemp == nodeIDroot){
        nodeIDlevel = nodeIDpred;
        nodeIDlevel.insert(nodeIDlevel.end(), nodeIDnext.begin(), nodeIDnext.end());
        it_bool = nodeIDs_samplesIDs_inb.insert(std::pair<std::vector<size_t>, std::vector<size_t>> (nodeIDlevel, {i}));
        if (!it_bool.second){
          it_bool.first->second.push_back(i);
        }
      }
      
      nodeIDtemp = nodeIDnext;
      nodeIDnext = {};
      
    }
    
  }
  
  // For each out-of-bag sample start in root, drop down the tree and return node set at each level
  for (auto & i : oob_sampleIDs) {
    
    nodeIDpredtemp = nodeIDroot;
    nodeIDtemp = nodeIDroot;
    nodeIDnext = {};
    nodeIDpred = {};
    bool next_level = true;
    
    while (next_level) {
      
      for (auto & nodeID : nodeIDtemp){
        if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
          nodeIDpred.push_back(nodeID);
        }else{
          size_t split_varID = split_varIDs[nodeID];
          // tree path depends whether splitting variable is in varU or not
          if (find(varU.begin(), varU.end(), split_varID) != varU.end()){
            // Move to child if splitting variable is in varU
            double value = prediction_data->get_x(i, split_varID);
            if (prediction_data->isOrderedVariable(split_varID)) {
              if (value <= split_values[nodeID]) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            } else {
              size_t factorID = floor(value) - 1;
              size_t splitID = floor(split_values[nodeID]);
              // Left if 0 found at position factorID
              if (!(splitID & (1ULL << factorID))) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            }
          }else{
            // When splitting on variable not in varU, move to both left and right children
            nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
            nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
          }
        }
      }
      
      nodeIDlevel = nodeIDpred;
      nodeIDlevel.insert(nodeIDlevel.end(), nodeIDnext.begin(), nodeIDnext.end());
      it = nodeIDs_samplesIDs_inb.find(nodeIDlevel);
      if (it == nodeIDs_samplesIDs_inb.end() || nodeIDnext.size() == 0){
        next_level = false;
        it_bool = nodeIDs_samplesIDs_oob.insert(std::pair<std::vector<size_t>, std::vector<size_t>> (nodeIDpredtemp, {i}));
        if (!it_bool.second){
          it_bool.first->second.push_back(i);
        }
      }else{
        nodeIDpredtemp = nodeIDlevel;
        nodeIDtemp = nodeIDnext;
        nodeIDnext = {};
      }
      
    }
    
  }
  
  // Generate predictions from the obtained projected partition
  int nsample = prediction_data->getNumRows();
  std::vector<double> pred_oob(nsample, 0);
  std::vector<size_t> nodeIDs;
  for (std::map<std::vector<size_t>, std::vector<size_t>>::iterator it = nodeIDs_samplesIDs_oob.begin(); it != nodeIDs_samplesIDs_oob.end(); ++it){
    nodeIDs = it->first;
    std::vector<size_t> sampleIDs_inb = nodeIDs_samplesIDs_inb[nodeIDs];
    double response = 0;
    size_t inb_size = 0;
    for (auto & id : sampleIDs_inb){
      size_t size_temp = sampleIDs_count[id];
      response += prediction_data->get_y(id, 0)*size_temp;
      inb_size += size_temp;
    }
    response = response/inb_size;
    for (auto & id : it->second){
      pred_oob[id] = response;
    }
  }
  
  return(pred_oob);
  
}

std::vector<double> Tree::predictOobProjectedVarj(size_t varj, const Data* prediction_data) {
  
  // Initialize variables
  std::map<std::vector<size_t>, std::vector<size_t>> nodeIDs_samplesIDs_inb;
  std::map<std::vector<size_t>, std::vector<size_t>> nodeIDs_samplesIDs_oob;
  std::map<std::vector<size_t>, std::vector<size_t>>::iterator it;
  std::pair<std::map<std::vector<size_t>, std::vector<size_t>>::iterator, bool> it_bool;
  std::vector<size_t> sampleIDs_unique;
  std::map<size_t, size_t> sampleIDs_count;
  std::pair<std::map<size_t, size_t>::iterator, bool> itIDs; 
  for (auto & i : sampleIDs){
    itIDs = sampleIDs_count.insert(std::pair<size_t, size_t> (i, 1));
    if (!itIDs.second){
      itIDs.first->second += 1;
    }else{
      sampleIDs_unique.push_back(i);
    }
  }
  std::vector<size_t> nodeIDroot = {0};
  std::vector<size_t> nodeIDtemp;
  std::vector<size_t> nodeIDnext;
  std::vector<size_t> nodeIDpred;
  std::vector<size_t> nodeIDlevel;
  std::vector<size_t> nodeIDpredtemp;
  
  // For each in-bag sample start in root, drop down the tree and return node set at each level
  for (auto & i : sampleIDs_unique) {
    
    nodeIDtemp = nodeIDroot;
    nodeIDnext = {};
    nodeIDpred = {};
    
    while (nodeIDtemp.size() > 0) {
      
      for (auto & nodeID : nodeIDtemp){
        if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
          nodeIDpred.push_back(nodeID);
        }else{
          size_t split_varID = split_varIDs[nodeID];
          // tree path depends whether splitting variable is varj or not
          if (split_varID != varj){
            // Move to child if splitting variable is not varj
            double value = prediction_data->get_x(i, split_varID);
            if (prediction_data->isOrderedVariable(split_varID)) {
              if (value <= split_values[nodeID]) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            } else {
              size_t factorID = floor(value) - 1;
              size_t splitID = floor(split_values[nodeID]);
              // Left if 0 found at position factorID
              if (!(splitID & (1ULL << factorID))) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            }
          }else{
            // When splitting on j, move to both left and right children
            nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
            nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
          }
        }
      }
      
      if (nodeIDnext.size() > 0 || nodeIDtemp == nodeIDroot){
        nodeIDlevel = nodeIDpred;
        nodeIDlevel.insert(nodeIDlevel.end(), nodeIDnext.begin(), nodeIDnext.end());
        it_bool = nodeIDs_samplesIDs_inb.insert(std::pair<std::vector<size_t>, std::vector<size_t>> (nodeIDlevel, {i}));
        if (!it_bool.second){
          it_bool.first->second.push_back(i);
        }
      }
      
      nodeIDtemp = nodeIDnext;
      nodeIDnext = {};
      
    }
    
  }
  
  // For each out-of-bag sample start in root, drop down the tree and return node set at each level
  for (auto & i : oob_sampleIDs) {
    
    nodeIDpredtemp = nodeIDroot;
    nodeIDtemp = nodeIDroot;
    nodeIDnext = {};
    nodeIDpred = {};
    bool next_level = true;
    
    while (next_level) {
      
      for (auto & nodeID : nodeIDtemp){
        if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
          nodeIDpred.push_back(nodeID);
        }else{
          size_t split_varID = split_varIDs[nodeID];
          // tree path depends whether splitting variable is varj or not
          if (split_varID != varj){
            // Move to child if splitting variable is not varj
            double value = prediction_data->get_x(i, split_varID);
            if (prediction_data->isOrderedVariable(split_varID)) {
              if (value <= split_values[nodeID]) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            } else {
              size_t factorID = floor(value) - 1;
              size_t splitID = floor(split_values[nodeID]);
              // Left if 0 found at position factorID
              if (!(splitID & (1ULL << factorID))) {
                // Move to left child
                nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
              } else {
                // Move to right child
                nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
              }
            }
          }else{
            // When splitting on j, move to both left and right children
            nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
            nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
          }
        }
      }
      
      nodeIDlevel = nodeIDpred;
      nodeIDlevel.insert(nodeIDlevel.end(), nodeIDnext.begin(), nodeIDnext.end());
      it = nodeIDs_samplesIDs_inb.find(nodeIDlevel);
      if (it == nodeIDs_samplesIDs_inb.end() || nodeIDnext.size() == 0){
        next_level = false;
        it_bool = nodeIDs_samplesIDs_oob.insert(std::pair<std::vector<size_t>, std::vector<size_t>> (nodeIDpredtemp, {i}));
        if (!it_bool.second){
          it_bool.first->second.push_back(i);
        }
      }else{
        nodeIDpredtemp = nodeIDlevel;
        nodeIDtemp = nodeIDnext;
        nodeIDnext = {};
      }
      
    }
    
  }
  
  // Generate predictions from the obtained projected partition
  int nsample = prediction_data->getNumRows();
  std::vector<double> pred_oob(nsample, 0);
  std::vector<size_t> nodeIDs;
  for (std::map<std::vector<size_t>, std::vector<size_t>>::iterator it = nodeIDs_samplesIDs_oob.begin(); it != nodeIDs_samplesIDs_oob.end(); ++it){
    nodeIDs = it->first;
    std::vector<size_t> sampleIDs_inb = nodeIDs_samplesIDs_inb[nodeIDs];
    double response = 0;
    size_t inb_size = 0;
    for (auto & id : sampleIDs_inb){
      size_t size_temp = sampleIDs_count[id];
      response += prediction_data->get_y(id, 0)*size_temp;
      inb_size += size_temp;
    }
    response = response/inb_size;
    for (auto & id : it->second){
      pred_oob[id] = response;
    }
  }
  
  return(pred_oob);
  
}

std::vector<std::vector<double>> Tree::predictOobProjectedHighDim(const Data* prediction_data) {
  
  // Initialize variables
  std::vector<std::map<std::vector<size_t>, std::vector<size_t>>> nodeIDs_samplesIDs_inb;
  std::vector<std::map<std::vector<size_t>, std::vector<size_t>>> nodeIDs_samplesIDs_oob;
  std::map<std::vector<size_t>, std::vector<size_t>> nodeIDs_samplesIDs;
  size_t p = prediction_data->getNumCols();
  std::vector<size_t> all_varj;
  for (size_t j = 0; j < p; ++j){
    nodeIDs_samplesIDs_inb.push_back(nodeIDs_samplesIDs);
    nodeIDs_samplesIDs_oob.push_back(nodeIDs_samplesIDs);
    all_varj.push_back(j);
  }
  std::map<std::vector<size_t>, std::vector<size_t>>::iterator it;
  std::pair<std::map<std::vector<size_t>, std::vector<size_t>>::iterator, bool> it_bool;
  std::vector<size_t> sampleIDs_unique;
  std::map<size_t, size_t> sampleIDs_count;
  std::pair<std::map<size_t, size_t>::iterator, bool> itIDs; 
  for (auto & i : sampleIDs){
    itIDs = sampleIDs_count.insert(std::pair<size_t, size_t> (i, 1));
    if (!itIDs.second){
      itIDs.first->second += 1;
    }else{
      sampleIDs_unique.push_back(i);
    }
  }
  std::vector<size_t> nodeIDroot = {0};
  std::vector<size_t> nodeIDtemp;
  std::vector<size_t> nodeIDnext;
  std::vector<size_t> nodeIDpred;
  std::vector<size_t> nodeIDlevel;
  std::vector<size_t> nodeIDpredtemp;
  size_t nodeIDorigin;
  std::vector<size_t> sampleID_varj;
  std::vector<size_t> empty_varj;
  
  // For each in-bag sample start in root, drop down the tree and return node set at each level
  for (auto & i : sampleIDs_unique) {
    
    sampleID_varj.clear();
    empty_varj.clear();
    bool is_terminal = false;
    nodeIDorigin = 0;

    while (!is_terminal){
      
      if (child_nodeIDs[0][nodeIDorigin] == 0 && child_nodeIDs[1][nodeIDorigin] == 0) {
        is_terminal = true;
      }else{
        nodeIDtemp.clear();
        nodeIDtemp.push_back(nodeIDorigin);
        size_t varj = split_varIDs[nodeIDorigin];
        double value = prediction_data->get_x(i, varj);
        if (prediction_data->isOrderedVariable(varj)) {
          if (value <= split_values[nodeIDorigin]) {
            // Move to left child
            nodeIDorigin = child_nodeIDs[0][nodeIDorigin];
          } else {
            // Move to right child
            nodeIDorigin = child_nodeIDs[1][nodeIDorigin];
          }
        } else {
          size_t factorID = floor(value) - 1;
          size_t splitID = floor(split_values[nodeIDorigin]);
          // Left if 0 found at position factorID
          if (!(splitID & (1ULL << factorID))) {
            // Move to left child
            nodeIDorigin = child_nodeIDs[0][nodeIDorigin];
          } else {
            // Move to right child
            nodeIDorigin = child_nodeIDs[1][nodeIDorigin];
          }
        }
        
        if (find(sampleID_varj.begin(), sampleID_varj.end(), varj) == sampleID_varj.end()){
          
          sampleID_varj.push_back(varj);
          nodeIDnext = {};
          nodeIDpred = {};
  
          while (nodeIDtemp.size() > 0) {
  
            for (auto & nodeID : nodeIDtemp){
              if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
                nodeIDpred.push_back(nodeID);
              }else{
                size_t split_varID = split_varIDs[nodeID];
                // tree path depends whether splitting variable is varj or not
                if (split_varID != varj){
                  // Move to child if splitting variable is not varj
                  double value = prediction_data->get_x(i, split_varID);
                  if (prediction_data->isOrderedVariable(split_varID)) {
                    if (value <= split_values[nodeID]) {
                      // Move to left child
                      nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
                    } else {
                      // Move to right child
                      nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
                    }
                  } else {
                    size_t factorID = floor(value) - 1;
                    size_t splitID = floor(split_values[nodeID]);
                    // Left if 0 found at position factorID
                    if (!(splitID & (1ULL << factorID))) {
                      // Move to left child
                      nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
                    } else {
                      // Move to right child
                      nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
                    }
                  }
                }else{
                  // When splitting on j, move to both left and right children
                  nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
                  nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
                }
              }
            }
  
            if (nodeIDnext.size() > 0 || nodeIDtemp == nodeIDroot){
              nodeIDlevel = nodeIDpred;
              nodeIDlevel.insert(nodeIDlevel.end(), nodeIDnext.begin(), nodeIDnext.end());
              it_bool = nodeIDs_samplesIDs_inb[varj].insert(std::pair<std::vector<size_t>, std::vector<size_t>> (nodeIDlevel, {i}));
              if (!it_bool.second){
                it_bool.first->second.push_back(i);
              }
            }
  
            nodeIDtemp = nodeIDnext;
            nodeIDnext = {};
  
          }
          
        }

      }
      
    }

  }

  // For each out-of-bag sample start in root, drop down the tree and return node set at each level
  std::map<size_t, std::vector<size_t>> leaves_varj;
  std::map<size_t, std::vector<size_t>> leaves_sampleIDs;
  std::pair<std::map<size_t, std::vector<size_t>>::iterator, bool> it_leaves;
  for (auto & i : oob_sampleIDs) {

    sampleID_varj.clear();
    empty_varj.clear();
    bool is_terminal = false;
    nodeIDorigin = 0;
    while (!is_terminal){
      if (child_nodeIDs[0][nodeIDorigin] == 0 && child_nodeIDs[1][nodeIDorigin] == 0) {
        is_terminal = true;
      }else{
        nodeIDtemp.clear();
        nodeIDtemp.push_back(nodeIDorigin);
        size_t varj = split_varIDs[nodeIDorigin];
        double value = prediction_data->get_x(i, varj);
        if (prediction_data->isOrderedVariable(varj)) {
          if (value <= split_values[nodeIDorigin]) {
            // Move to left child
            nodeIDorigin = child_nodeIDs[0][nodeIDorigin];
          } else {
            // Move to right child
            nodeIDorigin = child_nodeIDs[1][nodeIDorigin];
          }
        } else {
          size_t factorID = floor(value) - 1;
          size_t splitID = floor(split_values[nodeIDorigin]);
          // Left if 0 found at position factorID
          if (!(splitID & (1ULL << factorID))) {
            // Move to left child
            nodeIDorigin = child_nodeIDs[0][nodeIDorigin];
          } else {
            // Move to right child
            nodeIDorigin = child_nodeIDs[1][nodeIDorigin];
          }
        }
        
        if (find(sampleID_varj.begin(), sampleID_varj.end(), varj) == sampleID_varj.end()){
          
          sampleID_varj.push_back(varj);
          nodeIDnext = {};
          nodeIDpred = {};
          nodeIDpredtemp = nodeIDtemp;
          bool next_level = true;
  
          while (next_level) {
  
            for (auto & nodeID : nodeIDtemp){
              if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
                nodeIDpred.push_back(nodeID);
              }else{
                size_t split_varID = split_varIDs[nodeID];
                // tree path depends whether splitting variable is varj or not
                if (split_varID != varj){
                  // Move to child if splitting variable is not varj
                  double value = prediction_data->get_x(i, split_varID);
                  if (prediction_data->isOrderedVariable(split_varID)) {
                    if (value <= split_values[nodeID]) {
                      // Move to left child
                      nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
                    } else {
                      // Move to right child
                      nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
                    }
                  } else {
                    size_t factorID = floor(value) - 1;
                    size_t splitID = floor(split_values[nodeID]);
                    // Left if 0 found at position factorID
                    if (!(splitID & (1ULL << factorID))) {
                      // Move to left child
                      nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
                    } else {
                      // Move to right child
                      nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
                    }
                  }
                }else{
                  // When splitting on j, move to both left and right children
                  nodeIDnext.push_back(child_nodeIDs[0][nodeID]);
                  nodeIDnext.push_back(child_nodeIDs[1][nodeID]);
                }
              }
            }
  
            nodeIDlevel = nodeIDpred;
            nodeIDlevel.insert(nodeIDlevel.end(), nodeIDnext.begin(), nodeIDnext.end());
            it = nodeIDs_samplesIDs_inb[varj].find(nodeIDlevel);
            if (it == nodeIDs_samplesIDs_inb[varj].end() || nodeIDnext.size() == 0){
              next_level = false;
              it_bool = nodeIDs_samplesIDs_oob[varj].insert(std::pair<std::vector<size_t>, std::vector<size_t>> (nodeIDpredtemp, {i}));
              if (!it_bool.second){
                it_bool.first->second.push_back(i);
              }
            }else{
              nodeIDpredtemp = nodeIDlevel;
              nodeIDtemp = nodeIDnext;
              nodeIDnext = {};
            }
  
          }
          
        }
      }
    }

    leaves_varj.insert(std::pair<size_t, std::vector<size_t>> (nodeIDorigin, sampleID_varj));
    it_leaves = leaves_sampleIDs.insert(std::pair<size_t, std::vector<size_t>> (nodeIDorigin, {i}));
    if (!it_leaves.second){
      it_leaves.first->second.push_back(i);
    }
    
  }
  
  // Generate predictions from the obtained projected partition
  int nsample = prediction_data->getNumRows();
  std::vector<std::vector<double>> pred_oob_p;
  std::map<std::vector<size_t>, double> nodeIDs_response;
  std::pair<std::map<std::vector<size_t>, double>::iterator, bool> it_response;
  std::vector<size_t> nodeIDs;
  double response;
  for (size_t j = 0; j < p; ++j){
    std::vector<double> pred_oob(nsample, 0);
    for (std::map<std::vector<size_t>, std::vector<size_t>>::iterator it = nodeIDs_samplesIDs_oob[j].begin(); it != nodeIDs_samplesIDs_oob[j].end(); ++it){
      nodeIDs = it->first;
      it_response = nodeIDs_response.insert(std::pair<std::vector<size_t>, double> (nodeIDs, 0));
      if (it_response.second){
        std::vector<size_t> sampleIDs_inb = nodeIDs_samplesIDs_inb[j][nodeIDs];
        response = 0;
        size_t inb_size = 0;
        for (auto & id : sampleIDs_inb){
          size_t size_temp = sampleIDs_count[id];
          response += prediction_data->get_y(id, 0)*size_temp;
          inb_size += size_temp;
        }
        response = response/inb_size;
        it_response.first->second = response;
      }else{
        response = it_response.first->second;
      }
      for (auto & id : it->second){
        pred_oob[id] = response;
      }
    }
    pred_oob_p.push_back(pred_oob);
  }
  for (std::map<size_t, std::vector<size_t>>::iterator it = leaves_varj.begin(); it != leaves_varj.end(); ++it){
    response = split_values[it->first];
    for (size_t j = 0; j < p; ++j){
      if (find(it->second.begin(), it->second.end(), j) == it->second.end()){
        for (auto & i : leaves_sampleIDs[it->first]){
          pred_oob_p[j][i] = response;
        }
      }
    }
  }
  return(pred_oob_p);
  
}

std::vector<double> Tree::predictOobPermVarj(size_t j, const Data* prediction_data) {
  
  size_t num_nodes = child_nodeIDs[1].size();
  std::vector<std::vector<size_t>> nodeIDs_samplesIDs(num_nodes, std::vector<size_t> {});
  
  // For each training sample: start in root, drop down the tree and return terminal nodeID
  for (auto & sample_idx : sampleIDs) {
    
    size_t nodeID = 0;
    while (1) {
      
      // Break if terminal node
      if (child_nodeIDs[0][nodeID] == 0 && child_nodeIDs[1][nodeID] == 0) {
        break;
      }
      
      // Move to child
      size_t split_varID = split_varIDs[nodeID];
      
      double value = prediction_data->get_x(sample_idx, split_varID);
      if (prediction_data->isOrderedVariable(split_varID)) {
        if (value <= split_values[nodeID]) {
          // Move to left child
          nodeID = child_nodeIDs[0][nodeID];
        } else {
          // Move to right child
          nodeID = child_nodeIDs[1][nodeID];
        }
      } else {
        size_t factorID = floor(value) - 1;
        size_t splitID = floor(split_values[nodeID]);
        
        // Left if 0 found at position factorID
        if (!(splitID & (1ULL << factorID))) {
          // Move to left child
          nodeID = child_nodeIDs[0][nodeID];
        } else {
          // Move to right child
          nodeID = child_nodeIDs[1][nodeID];
        }
      }
    }
    
    nodeIDs_samplesIDs[nodeID].push_back(sample_idx);
  }
  
  // Recompute output average in each terminal cell of tree
  std::vector<double> nodeIDs_meanY(num_nodes, 0);
  for (size_t i = 0; i < num_nodes; ++i){
    if (nodeIDs_samplesIDs[i].size() > 0){
      for (auto & ind : nodeIDs_samplesIDs[i]){
        nodeIDs_meanY[i] += data->get_y(ind, 0);
      }
      nodeIDs_meanY[i] = nodeIDs_meanY[i]/nodeIDs_samplesIDs[i].size();
    }
  }
  
  // For each oob sample, start in root, drop down the tree and return weighted output mean of the terminal cell
  std::vector<double> pred_oob(num_samples, 0);
  
  // Permute OOB sample
  std::vector<size_t> permutations;
  for (size_t i=0; i < num_samples_oob; i++){
    permutations.push_back(i);
  }
  std::shuffle(permutations.begin(), permutations.end(), random_number_generator);
  
  // For each sample, drop down the tree and add prediction
  //for (auto & sample_idx : oob_sampleIDs){
  for (size_t i=0; i < num_samples_oob; i++){
    size_t nodeID = dropDownSamplePermuted(j, oob_sampleIDs[i], permutations[i]);
    pred_oob[oob_sampleIDs[i]] = nodeIDs_meanY[nodeID];
  }
  
  
  return(pred_oob);
  
}

// #nocov start
void Tree::appendToFile(std::ofstream& file) {

  // Save general fields
  saveVector2D(child_nodeIDs, file);
  saveVector1D(split_varIDs, file);
  saveVector1D(split_values, file);

  // Call special functions for subclasses to save special fields.
  appendToFileInternal(file);
}
// #nocov end

void Tree::createPossibleSplitVarSubset(std::vector<size_t>& result) {

  size_t num_vars = data->getNumCols();

  // For corrected Gini importance add dummy variables
  if (importance_mode == IMP_GINI_CORRECTED) {
    num_vars += data->getNumCols();
  }

  // Randomly add non-deterministic variables (according to weights if needed)
  if (split_select_weights->empty()) {
    if (deterministic_varIDs->empty()) {
      drawWithoutReplacement(result, random_number_generator, num_vars, mtry);
    } else {
      drawWithoutReplacementSkip(result, random_number_generator, num_vars, (*deterministic_varIDs), mtry);
    }
  } else {
    drawWithoutReplacementWeighted(result, random_number_generator, num_vars, mtry, *split_select_weights);
  }

  // Always use deterministic variables
  std::copy(deterministic_varIDs->begin(), deterministic_varIDs->end(), std::inserter(result, result.end()));
}

bool Tree::splitNode(size_t nodeID) {

  // Select random subset of variables to possibly split at
  std::vector<size_t> possible_split_varIDs;
  createPossibleSplitVarSubset(possible_split_varIDs);

  // Call subclass method, sets split_varIDs and split_values
  bool stop = splitNodeInternal(nodeID, possible_split_varIDs);
  if (stop) {
    // Terminal node
    return true;
  }

  size_t split_varID = split_varIDs[nodeID];
  double split_value = split_values[nodeID];

  // Save non-permuted variable for prediction
  split_varIDs[nodeID] = data->getUnpermutedVarID(split_varID);

  // Create child nodes
  size_t left_child_nodeID = split_varIDs.size();
  child_nodeIDs[0][nodeID] = left_child_nodeID;
  createEmptyNode();
  start_pos[left_child_nodeID] = start_pos[nodeID];

  size_t right_child_nodeID = split_varIDs.size();
  child_nodeIDs[1][nodeID] = right_child_nodeID;
  createEmptyNode();
  start_pos[right_child_nodeID] = end_pos[nodeID];

  // For each sample in node, assign to left or right child
  double left_size = 0;
  double right_size = 0;
  if (data->isOrderedVariable(split_varID)) {
    // Ordered: left is <= splitval and right is > splitval
    size_t pos = start_pos[nodeID];
    while (pos < start_pos[right_child_nodeID]) {
      size_t sampleID = sampleIDs[pos];
      if (data->get_x(sampleID, split_varID) <= split_value) {
        // If going to left, do nothing
        ++pos;
        ++left_size;
      } else {
        // If going to right, move to right end
        --start_pos[right_child_nodeID];
        std::swap(sampleIDs[pos], sampleIDs[start_pos[right_child_nodeID]]);
        ++right_size;
      }
    }
  } else {
    // Unordered: If bit at position is 1 -> right, 0 -> left
    size_t pos = start_pos[nodeID];
    while (pos < start_pos[right_child_nodeID]) {
      size_t sampleID = sampleIDs[pos];
      double level = data->get_x(sampleID, split_varID);
      size_t factorID = floor(level) - 1;
      size_t splitID = floor(split_value);

      // Left if 0 found at position factorID
      if (!(splitID & (1ULL << factorID))) {
        // If going to left, do nothing
        ++pos;
        ++left_size;
      } else {
        // If going to right, move to right end
        --start_pos[right_child_nodeID];
        std::swap(sampleIDs[pos], sampleIDs[start_pos[right_child_nodeID]]);
        ++right_size;
      }
    }
  }

  // End position of left child is start position of right child
  end_pos[left_child_nodeID] = start_pos[right_child_nodeID];
  end_pos[right_child_nodeID] = end_pos[nodeID];
  
  // compute weight
  child_weights[0][nodeID] = left_size/(left_size + right_size);
  child_weights[1][nodeID] = right_size/(left_size + right_size);

  // No terminal node
  return false;
}

void Tree::createEmptyNode() {
  split_varIDs.push_back(0);
  split_values.push_back(0);
  child_nodeIDs[0].push_back(0);
  child_nodeIDs[1].push_back(0);
  start_pos.push_back(0);
  end_pos.push_back(0);
  child_weights[0].push_back(0);
  child_weights[1].push_back(0);

  createEmptyNodeInternal();
}

size_t Tree::dropDownSamplePermuted(size_t permuted_varID, size_t sampleID, size_t permuted_sampleID) {

  // Start in root and drop down
  size_t nodeID = 0;
  while (child_nodeIDs[0][nodeID] != 0 || child_nodeIDs[1][nodeID] != 0) {

    // Permute if variable is permutation variable
    size_t split_varID = split_varIDs[nodeID];
    size_t sampleID_final = sampleID;
    if (split_varID == permuted_varID) {
      sampleID_final = permuted_sampleID;
    }

    // Move to child
    double value = data->get_x(sampleID_final, split_varID);
    if (data->isOrderedVariable(split_varID)) {
      if (value <= split_values[nodeID]) {
        // Move to left child
        nodeID = child_nodeIDs[0][nodeID];
      } else {
        // Move to right child
        nodeID = child_nodeIDs[1][nodeID];
      }
    } else {
      size_t factorID = floor(value) - 1;
      size_t splitID = floor(split_values[nodeID]);

      // Left if 0 found at position factorID
      if (!(splitID & (1ULL << factorID))) {
        // Move to left child
        nodeID = child_nodeIDs[0][nodeID];
      } else {
        // Move to right child
        nodeID = child_nodeIDs[1][nodeID];
      }
    }

  }
  return nodeID;
}

void Tree::permuteAndPredictOobSamples(size_t permuted_varID, std::vector<size_t>& permutations) {

  // Permute OOB sample
  //std::vector<size_t> permutations(oob_sampleIDs);
  std::shuffle(permutations.begin(), permutations.end(), random_number_generator);

  // For each sample, drop down the tree and add prediction
  for (size_t i = 0; i < num_samples_oob; ++i) {
    size_t nodeID = dropDownSamplePermuted(permuted_varID, oob_sampleIDs[i], permutations[i]);
    prediction_terminal_nodeIDs[i] = nodeID;
  }
}

void Tree::bootstrap() {

  // Use fraction (default 63.21%) of the samples
  size_t num_samples_inbag = (size_t) num_samples * (*sample_fraction)[0];

  // Reserve space, reserve a little more to be save)
  sampleIDs.reserve(num_samples_inbag);
  oob_sampleIDs.reserve(num_samples * (exp(-(*sample_fraction)[0]) + 0.1));

  std::uniform_int_distribution<size_t> unif_dist(0, num_samples - 1);

  // Start with all samples OOB
  inbag_counts.resize(num_samples, 0);

  // Draw num_samples samples with replacement (num_samples_inbag out of n) as inbag and mark as not OOB
  for (size_t s = 0; s < num_samples_inbag; ++s) {
    size_t draw = unif_dist(random_number_generator);
    sampleIDs.push_back(draw);
    ++inbag_counts[draw];
  }

  // Save OOB samples
  for (size_t s = 0; s < inbag_counts.size(); ++s) {
    if (inbag_counts[s] == 0) {
      oob_sampleIDs.push_back(s);
    }
  }
  num_samples_oob = oob_sampleIDs.size();

  if (!keep_inbag) {
    inbag_counts.clear();
    inbag_counts.shrink_to_fit();
  }
}

void Tree::bootstrapWeighted() {

  // Use fraction (default 63.21%) of the samples
  size_t num_samples_inbag = (size_t) num_samples * (*sample_fraction)[0];

  // Reserve space, reserve a little more to be save)
  sampleIDs.reserve(num_samples_inbag);
  oob_sampleIDs.reserve(num_samples * (exp(-(*sample_fraction)[0]) + 0.1));

  std::discrete_distribution<> weighted_dist(case_weights->begin(), case_weights->end());

  // Start with all samples OOB
  inbag_counts.resize(num_samples, 0);

  // Draw num_samples samples with replacement (n out of n) as inbag and mark as not OOB
  for (size_t s = 0; s < num_samples_inbag; ++s) {
    size_t draw = weighted_dist(random_number_generator);
    sampleIDs.push_back(draw);
    ++inbag_counts[draw];
  }

  // Save OOB samples. In holdout mode these are the cases with 0 weight.
  if (holdout) {
    for (size_t s = 0; s < (*case_weights).size(); ++s) {
      if ((*case_weights)[s] == 0) {
        oob_sampleIDs.push_back(s);
      }
    }
  } else {
    for (size_t s = 0; s < inbag_counts.size(); ++s) {
      if (inbag_counts[s] == 0) {
        oob_sampleIDs.push_back(s);
      }
    }
  }
  num_samples_oob = oob_sampleIDs.size();

  if (!keep_inbag) {
    inbag_counts.clear();
    inbag_counts.shrink_to_fit();
  }
}

void Tree::bootstrapWithoutReplacement() {

  // Use fraction (default 63.21%) of the samples
  size_t num_samples_inbag = (size_t) num_samples * (*sample_fraction)[0];
  shuffleAndSplit(sampleIDs, oob_sampleIDs, num_samples, num_samples_inbag, random_number_generator);
  num_samples_oob = oob_sampleIDs.size();

  if (keep_inbag) {
    // All observation are 0 or 1 times inbag
    inbag_counts.resize(num_samples, 1);
    for (size_t i = 0; i < oob_sampleIDs.size(); i++) {
      inbag_counts[oob_sampleIDs[i]] = 0;
    }
  }
}

void Tree::bootstrapWithoutReplacementWeighted() {

  // Use fraction (default 63.21%) of the samples
  size_t num_samples_inbag = (size_t) num_samples * (*sample_fraction)[0];
  drawWithoutReplacementWeighted(sampleIDs, random_number_generator, num_samples - 1, num_samples_inbag, *case_weights);

  // All observation are 0 or 1 times inbag
  inbag_counts.resize(num_samples, 0);
  for (auto& sampleID : sampleIDs) {
    inbag_counts[sampleID] = 1;
  }

  // Save OOB samples. In holdout mode these are the cases with 0 weight.
  if (holdout) {
    for (size_t s = 0; s < (*case_weights).size(); ++s) {
      if ((*case_weights)[s] == 0) {
        oob_sampleIDs.push_back(s);
      }
    }
  } else {
    for (size_t s = 0; s < inbag_counts.size(); ++s) {
      if (inbag_counts[s] == 0) {
        oob_sampleIDs.push_back(s);
      }
    }
  }
  num_samples_oob = oob_sampleIDs.size();

  if (!keep_inbag) {
    inbag_counts.clear();
    inbag_counts.shrink_to_fit();
  }
}

void Tree::bootstrapClassWise() {
  // Empty on purpose (virtual function only implemented in classification and probability)
}

void Tree::bootstrapWithoutReplacementClassWise() {
  // Empty on purpose (virtual function only implemented in classification and probability)
}

void Tree::setManualInbag() {
  // Select observation as specified in manual_inbag vector
  sampleIDs.reserve(manual_inbag->size());
  inbag_counts.resize(num_samples, 0);
  for (size_t i = 0; i < manual_inbag->size(); ++i) {
    size_t inbag_count = (*manual_inbag)[i];
    if ((*manual_inbag)[i] > 0) {
      for (size_t j = 0; j < inbag_count; ++j) {
        sampleIDs.push_back(i);
      }
      inbag_counts[i] = inbag_count;
    } else {
      oob_sampleIDs.push_back(i);
    }
  }
  num_samples_oob = oob_sampleIDs.size();

  // Shuffle samples
  std::shuffle(sampleIDs.begin(), sampleIDs.end(), random_number_generator);

  if (!keep_inbag) {
    inbag_counts.clear();
    inbag_counts.shrink_to_fit();
  }
}

} // namespace shaff
