/*-------------------------------------------------------------------------------
 This file is part of Ranger.

 Copyright (c) [2014-2018] [Marvin N. Wright]

 This software may be modified and distributed under the terms of the MIT license.

 Please note that the C++ core of Ranger is distributed under MIT license and the
 R package "shaff" under GPL3 license.
 #-------------------------------------------------------------------------------*/

#include <math.h>
#include <algorithm>
#include <stdexcept>
#include <string>
#include <ctime>
#include <functional>
#ifndef OLD_WIN_R_BUILD
#include <thread>
#include <chrono>
#endif

#include "utility.h"
#include "Forest.h"
#include "DataChar.h"
#include "DataDouble.h"
#include "DataFloat.h"

namespace shaff {

Forest::Forest() :
    verbose_out(0), num_trees(DEFAULT_NUM_TREE), mtry(0), min_node_size(0), num_independent_variables(0), seed(0), num_samples(
        0), prediction_mode(false), memory_mode(MEM_DOUBLE), sample_with_replacement(true), memory_saving_splitting(
        false), splitrule(DEFAULT_SPLITRULE), predict_all(false), keep_inbag(false), sample_fraction( { 1 }), holdout(
        false), prediction_type(DEFAULT_PREDICTIONTYPE), num_random_splits(DEFAULT_NUM_RANDOM_SPLITS), max_depth(
        DEFAULT_MAXDEPTH), alpha(DEFAULT_ALPHA), minprop(DEFAULT_MINPROP), num_threads(DEFAULT_NUM_THREADS), data { }, overall_prediction_error(
    NAN), importance_mode(DEFAULT_IMPORTANCE_MODE), regularization_usedepth(false), progress(0) {
}

// #nocov start
void Forest::initCpp(std::string dependent_variable_name, MemoryMode memory_mode, std::string input_file, uint mtry,
    std::string output_prefix, uint num_trees, std::ostream* verbose_out, uint seed, uint num_threads,
    std::string load_forest_filename, ImportanceMode importance_mode, uint min_node_size,
    std::string split_select_weights_file, const std::vector<std::string>& always_split_variable_names,
    std::string status_variable_name, bool sample_with_replacement,
    const std::vector<std::string>& unordered_variable_names, bool memory_saving_splitting, SplitRule splitrule,
    std::string case_weights_file, bool predict_all, double sample_fraction, double alpha, double minprop, bool holdout,
    PredictionType prediction_type, uint num_random_splits, uint max_depth,
    const std::vector<double>& regularization_factor, bool regularization_usedepth) {

  this->verbose_out = verbose_out;

  if (!dependent_variable_name.empty()) {
    if (status_variable_name.empty()) {
      this->dependent_variable_names = {dependent_variable_name};
    } else {
      this->dependent_variable_names = {dependent_variable_name, status_variable_name};
    }
  }

  // Set prediction mode
  bool prediction_mode = false;
  if (!load_forest_filename.empty()) {
    prediction_mode = true;
  }

  // Sample fraction default and convert to vector
  if (sample_fraction == 0) {
    if (sample_with_replacement) {
      sample_fraction = DEFAULT_SAMPLE_FRACTION_REPLACE;
    } else {
      sample_fraction = DEFAULT_SAMPLE_FRACTION_NOREPLACE;
    }
  }
  std::vector<double> sample_fraction_vector = { sample_fraction };

  if (prediction_mode) {
    loadDependentVariableNamesFromFile(load_forest_filename);
  }

  // Call other init function
  init(memory_mode, loadDataFromFile(input_file), mtry, output_prefix, num_trees, seed, num_threads, importance_mode,
      min_node_size, prediction_mode, sample_with_replacement, unordered_variable_names, memory_saving_splitting,
      splitrule, predict_all, sample_fraction_vector, alpha, minprop, holdout, prediction_type, num_random_splits,
      false, max_depth, regularization_factor, regularization_usedepth);

  if (prediction_mode) {
    loadFromFile(load_forest_filename);
  }
  // Set variables to be always considered for splitting
  if (!always_split_variable_names.empty()) {
    setAlwaysSplitVariables(always_split_variable_names);
  }

  // TODO: Read 2d weights for tree-wise split select weights
  // Load split select weights from file
  if (!split_select_weights_file.empty()) {
    std::vector<std::vector<double>> split_select_weights;
    split_select_weights.resize(1);
    loadDoubleVectorFromFile(split_select_weights[0], split_select_weights_file);
    if (split_select_weights[0].size() != num_independent_variables) {
      throw std::runtime_error("Number of split select weights is not equal to number of independent variables.");
    }
    setSplitWeightVector(split_select_weights);
  }

  // Load case weights from file
  if (!case_weights_file.empty()) {
    loadDoubleVectorFromFile(case_weights, case_weights_file);
    if (case_weights.size() != num_samples) {
      throw std::runtime_error("Number of case weights is not equal to number of samples.");
    }
  }

  // Sample from non-zero weights in holdout mode
  if (holdout && !case_weights.empty()) {
    size_t nonzero_weights = 0;
    for (auto& weight : case_weights) {
      if (weight > 0) {
        ++nonzero_weights;
      }
    }
    this->sample_fraction[0] = this->sample_fraction[0] * ((double) nonzero_weights / (double) num_samples);
  }

  // Check if all catvars are coded in integers starting at 1
  if (!unordered_variable_names.empty()) {
    std::string error_message = checkUnorderedVariables(*data, unordered_variable_names);
    if (!error_message.empty()) {
      throw std::runtime_error(error_message);
    }
  }
}
// #nocov end

void Forest::initR(std::unique_ptr<Data> input_data, uint mtry, uint num_trees, std::ostream* verbose_out, uint seed,
    uint num_threads, ImportanceMode importance_mode, uint min_node_size,
    std::vector<std::vector<double>>& split_select_weights, const std::vector<std::string>& always_split_variable_names,
    bool prediction_mode, bool sample_with_replacement, const std::vector<std::string>& unordered_variable_names,
    bool memory_saving_splitting, SplitRule splitrule, std::vector<double>& case_weights,
    std::vector<std::vector<size_t>>& manual_inbag, bool predict_all, bool keep_inbag,
    std::vector<double>& sample_fraction, double alpha, double minprop, bool holdout, PredictionType prediction_type,
    uint num_random_splits, bool order_snps, uint max_depth, const std::vector<double>& regularization_factor,
    bool regularization_usedepth) {

  this->verbose_out = verbose_out;

  // Call other init function
  init(MEM_DOUBLE, std::move(input_data), mtry, "", num_trees, seed, num_threads, importance_mode, min_node_size,
      prediction_mode, sample_with_replacement, unordered_variable_names, memory_saving_splitting, splitrule,
      predict_all, sample_fraction, alpha, minprop, holdout, prediction_type, num_random_splits, order_snps, max_depth,
      regularization_factor, regularization_usedepth);

  // Set variables to be always considered for splitting
  if (!always_split_variable_names.empty()) {
    setAlwaysSplitVariables(always_split_variable_names);
  }

  // Set split select weights
  if (!split_select_weights.empty()) {
    setSplitWeightVector(split_select_weights);
  }

  // Set case weights
  if (!case_weights.empty()) {
    if (case_weights.size() != num_samples) {
      throw std::runtime_error("Number of case weights not equal to number of samples.");
    }
    this->case_weights = case_weights;
  }

  // Set manual inbag
  if (!manual_inbag.empty()) {
    this->manual_inbag = manual_inbag;
  }

  // Keep inbag counts
  this->keep_inbag = keep_inbag;
}

void Forest::init(MemoryMode memory_mode, std::unique_ptr<Data> input_data, uint mtry, std::string output_prefix,
    uint num_trees, uint seed, uint num_threads, ImportanceMode importance_mode, uint min_node_size,
    bool prediction_mode, bool sample_with_replacement, const std::vector<std::string>& unordered_variable_names,
    bool memory_saving_splitting, SplitRule splitrule, bool predict_all, std::vector<double>& sample_fraction,
    double alpha, double minprop, bool holdout, PredictionType prediction_type, uint num_random_splits, bool order_snps,
    uint max_depth, const std::vector<double>& regularization_factor, bool regularization_usedepth) {

  // Initialize data with memmode
  this->data = std::move(input_data);

  // Initialize random number generator and set seed
  if (seed == 0) {
    std::random_device random_device;
    random_number_generator.seed(random_device());
  } else {
    random_number_generator.seed(seed);
  }

  // Set number of threads
  if (num_threads == DEFAULT_NUM_THREADS) {
#ifdef OLD_WIN_R_BUILD
    this->num_threads = 1;
#else
    this->num_threads = std::thread::hardware_concurrency();
#endif
  } else {
    this->num_threads = num_threads;
  }

  // Set member variables
  this->num_trees = num_trees;
  this->mtry = mtry;
  this->seed = seed;
  this->output_prefix = output_prefix;
  this->importance_mode = importance_mode;
  this->min_node_size = min_node_size;
  this->memory_mode = memory_mode;
  this->prediction_mode = prediction_mode;
  this->sample_with_replacement = sample_with_replacement;
  this->memory_saving_splitting = memory_saving_splitting;
  this->splitrule = splitrule;
  this->predict_all = predict_all;
  this->sample_fraction = sample_fraction;
  this->holdout = holdout;
  this->alpha = alpha;
  this->minprop = minprop;
  this->prediction_type = prediction_type;
  this->num_random_splits = num_random_splits;
  this->max_depth = max_depth;
  this->regularization_factor = regularization_factor;
  this->regularization_usedepth = regularization_usedepth;

  // Set number of samples and variables
  num_samples = data->getNumRows();
  num_independent_variables = data->getNumCols();

  // Set unordered factor variables
  if (!prediction_mode) {
    data->setIsOrderedVariable(unordered_variable_names);
  }

  initInternal();

  // Init split select weights
  split_select_weights.push_back(std::vector<double>());

  // Init manual inbag
  manual_inbag.push_back(std::vector<size_t>());

  // Check if mtry is in valid range
  if (this->mtry > num_independent_variables) {
    throw std::runtime_error("mtry can not be larger than number of variables in data.");
  }

  // Check if any observations samples
  if ((size_t) num_samples * sample_fraction[0] < 1) {
    throw std::runtime_error("sample_fraction too small, no observations sampled.");
  }

  // Permute samples for corrected Gini importance
  if (importance_mode == IMP_GINI_CORRECTED) {
    data->permuteSampleIDs(random_number_generator);
  }

  // Order SNP levels if in "order" splitting
  if (!prediction_mode && order_snps) {
    data->orderSnpLevels((importance_mode == IMP_GINI_CORRECTED));
  }

  // Regularization
  if (regularization_factor.size() > 0) {
    if (regularization_factor.size() == 1 && num_independent_variables > 1) {
      double single_regularization_factor = regularization_factor[0];
      this->regularization_factor.resize(num_independent_variables, single_regularization_factor);
    } else if (regularization_factor.size() != num_independent_variables) {
      throw std::runtime_error("Use 1 or p (the number of predictor variables) regularization factors.");
    }

    // Set all variables to not used
    split_varIDs_used.resize(num_independent_variables, false);
  }
}

void Forest::run(bool verbose, bool compute_oob_error) {

  if (prediction_mode) {
    if (verbose && verbose_out) {
      *verbose_out << "Predicting .." << std::endl;
    }
    predict();
  } else {
    if (verbose && verbose_out) {
      *verbose_out << "Growing trees .." << std::endl;
    }

    grow();

    if (verbose && verbose_out) {
      *verbose_out << "Computing prediction error .." << std::endl;
    }

    if (compute_oob_error) {
      computePredictionError();
    }

    if (importance_mode == IMP_PERM_BREIMAN || importance_mode == IMP_PERM_LIAW || importance_mode == IMP_PERM_RAW
        || importance_mode == IMP_PERM_CASEWISE) {
      if (verbose && verbose_out) {
        *verbose_out << "Computing permutation variable importance .." << std::endl;
      }
      computePermutationImportance();
    }
  }
}

// #nocov start
void Forest::writeOutput() {

  if (verbose_out)
    *verbose_out << std::endl;
  writeOutputInternal();
  if (verbose_out) {
    if (dependent_variable_names.size() >= 1) {
      *verbose_out << "Dependent variable name:           " << dependent_variable_names[0] << std::endl;
    }
    *verbose_out << "Number of trees:                   " << num_trees << std::endl;
    *verbose_out << "Sample size:                       " << num_samples << std::endl;
    *verbose_out << "Number of independent variables:   " << num_independent_variables << std::endl;
    *verbose_out << "Mtry:                              " << mtry << std::endl;
    *verbose_out << "Target node size:                  " << min_node_size << std::endl;
    *verbose_out << "Variable importance mode:          " << importance_mode << std::endl;
    *verbose_out << "Memory mode:                       " << memory_mode << std::endl;
    *verbose_out << "Seed:                              " << seed << std::endl;
    *verbose_out << "Number of threads:                 " << num_threads << std::endl;
    *verbose_out << std::endl;
  }

  if (prediction_mode) {
    writePredictionFile();
  } else {
    if (verbose_out) {
      *verbose_out << "Overall OOB prediction error:      " << overall_prediction_error << std::endl;
      *verbose_out << std::endl;
    }

    if (!split_select_weights.empty() & !split_select_weights[0].empty()) {
      if (verbose_out) {
        *verbose_out
            << "Warning: Split select weights used. Variable importance measures are only comparable for variables with equal weights."
            << std::endl;
      }
    }

    if (importance_mode != IMP_NONE) {
      writeImportanceFile();
    }

    writeConfusionFile();
  }
}

void Forest::writeImportanceFile() {

  // Open importance file for writing
  std::string filename = output_prefix + ".importance";
  std::ofstream importance_file;
  importance_file.open(filename, std::ios::out);
  if (!importance_file.good()) {
    throw std::runtime_error("Could not write to importance file: " + filename + ".");
  }

  if (importance_mode == IMP_PERM_CASEWISE) {
    // Write variable names
    for (auto& variable_name : data->getVariableNames()) {
      importance_file << variable_name << " ";
    }
    importance_file << std::endl;

    // Write importance values
    for (size_t i = 0; i < num_samples; ++i) {
      for (size_t j = 0; j < num_independent_variables; ++j) {
        if (variable_importance_casewise.size() <= (j * num_samples + i)) {
          throw std::runtime_error("Memory error in local variable importance.");
        }
        importance_file << variable_importance_casewise[j * num_samples + i] << " ";
      }
      importance_file << std::endl;
    }
  } else {
    // Write importance to file
    for (size_t i = 0; i < variable_importance.size(); ++i) {
      std::string variable_name = data->getVariableNames()[i];
      importance_file << variable_name << ": " << variable_importance[i] << std::endl;
    }
  }

  importance_file.close();
  if (verbose_out)
    *verbose_out << "Saved variable importance to file " << filename << "." << std::endl;
}

void Forest::saveToFile() {

  // Open file for writing
  std::string filename = output_prefix + ".forest";
  std::ofstream outfile;
  outfile.open(filename, std::ios::binary);
  if (!outfile.good()) {
    throw std::runtime_error("Could not write to output file: " + filename + ".");
  }

  // Write dependent variable names
  uint num_dependent_variables = dependent_variable_names.size();
  if (num_dependent_variables >= 1) {
    outfile.write((char*) &num_dependent_variables, sizeof(num_dependent_variables));
    for (auto& var_name : dependent_variable_names) {
      size_t length = var_name.size();
      outfile.write((char*) &length, sizeof(length));
      outfile.write((char*) var_name.c_str(), length * sizeof(char));
    }
  } else {
    throw std::runtime_error("Missing dependent variable name.");
  }

  // Write num_trees
  outfile.write((char*) &num_trees, sizeof(num_trees));

  // Write is_ordered_variable
  saveVector1D(data->getIsOrderedVariable(), outfile);

  saveToFileInternal(outfile);

  // Write tree data for each tree
  for (auto& tree : trees) {
    tree->appendToFile(outfile);
  }

  // Close file
  outfile.close();
  if (verbose_out)
    *verbose_out << "Saved forest to file " << filename << "." << std::endl;
}
// #nocov end

void Forest::grow() {

  // Create thread ranges
  equalSplit(thread_ranges, 0, num_trees - 1, num_threads);

  // Call special grow functions of subclasses. There trees must be created.
  growInternal();

  // Init trees, create a seed for each tree, based on main seed
  std::uniform_int_distribution<uint> udist;
  for (size_t i = 0; i < num_trees; ++i) {
    uint tree_seed;
    if (seed == 0) {
      tree_seed = udist(random_number_generator);
    } else {
      tree_seed = (i + 1) * seed;
    }

    // Get split select weights for tree
    std::vector<double>* tree_split_select_weights;
    if (split_select_weights.size() > 1) {
      tree_split_select_weights = &split_select_weights[i];
    } else {
      tree_split_select_weights = &split_select_weights[0];
    }

    // Get inbag counts for tree
    std::vector<size_t>* tree_manual_inbag;
    if (manual_inbag.size() > 1) {
      tree_manual_inbag = &manual_inbag[i];
    } else {
      tree_manual_inbag = &manual_inbag[0];
    }

    trees[i]->init(data.get(), mtry, num_samples, tree_seed, &deterministic_varIDs, tree_split_select_weights,
        importance_mode, min_node_size, sample_with_replacement, memory_saving_splitting, splitrule, &case_weights,
        tree_manual_inbag, keep_inbag, &sample_fraction, alpha, minprop, holdout, num_random_splits, max_depth,
        &regularization_factor, regularization_usedepth, &split_varIDs_used);
  }

  // Init variable importance
  variable_importance.resize(num_independent_variables, 0);

  // Grow trees in multiple threads
#ifdef OLD_WIN_R_BUILD
  // #nocov start
  progress = 0;
  clock_t start_time = clock();
  clock_t lap_time = clock();
  for (size_t i = 0; i < num_trees; ++i) {
    trees[i]->grow(&variable_importance);
    progress++;
    showProgress("Growing trees..", start_time, lap_time);
  }
  // #nocov end
#else
  progress = 0;
#ifdef R_BUILD
  aborted = false;
  aborted_threads = 0;
#endif

  std::vector<std::thread> threads;
  threads.reserve(num_threads);

  // Initialize importance per thread
  std::vector<std::vector<double>> variable_importance_threads(num_threads);

  for (uint i = 0; i < num_threads; ++i) {
    if (importance_mode == IMP_GINI || importance_mode == IMP_GINI_CORRECTED) {
      variable_importance_threads[i].resize(num_independent_variables, 0);
    }
    threads.emplace_back(&Forest::growTreesInThread, this, i, &(variable_importance_threads[i]));
  }
  showProgress("Growing trees..", num_trees);
  for (auto &thread : threads) {
    thread.join();
  }

#ifdef R_BUILD
  if (aborted_threads > 0) {
    throw std::runtime_error("User interrupt.");
  }
#endif

  // Sum thread importances
  if (importance_mode == IMP_GINI || importance_mode == IMP_GINI_CORRECTED) {
    variable_importance.resize(num_independent_variables, 0);
    for (size_t i = 0; i < num_independent_variables; ++i) {
      for (uint j = 0; j < num_threads; ++j) {
        variable_importance[i] += variable_importance_threads[j][i];
      }
    }
    variable_importance_threads.clear();
  }

#endif

  // Divide importance by number of trees
  if (importance_mode == IMP_GINI || importance_mode == IMP_GINI_CORRECTED) {
    for (auto& v : variable_importance) {
      v /= num_trees;
    }
  }
}

void Forest::predict() {

  // Predict trees in multiple threads and join the threads with the main thread
#ifdef OLD_WIN_R_BUILD
  // #nocov start
  progress = 0;
  clock_t start_time = clock();
  clock_t lap_time = clock();
  for (size_t i = 0; i < num_trees; ++i) {
    trees[i]->predict(data.get(), false);
    progress++;
    showProgress("Predicting..", start_time, lap_time);
  }

  // For all samples get tree predictions
  allocatePredictMemory();
  for (size_t sample_idx = 0; sample_idx < data->getNumRows(); ++sample_idx) {
    predictInternal(sample_idx);
  }
  // #nocov end
#else
  progress = 0;
#ifdef R_BUILD
  aborted = false;
  aborted_threads = 0;
#endif

  // Predict
  std::vector<std::thread> threads;
  threads.reserve(num_threads);
  for (uint i = 0; i < num_threads; ++i) {
    threads.emplace_back(&Forest::predictTreesInThread, this, i, data.get(), false);
  }
  showProgress("Predicting..", num_trees);
  for (auto &thread : threads) {
    thread.join();
  }

  // Aggregate predictions
  allocatePredictMemory();
  threads.clear();
  threads.reserve(num_threads);
  progress = 0;
  for (uint i = 0; i < num_threads; ++i) {
    threads.emplace_back(&Forest::predictInternalInThread, this, i);
  }
  showProgress("Aggregating predictions..", num_samples);
  for (auto &thread : threads) {
    thread.join();
  }

#ifdef R_BUILD
  if (aborted_threads > 0) {
    throw std::runtime_error("User interrupt.");
  }
#endif
#endif
}

void Forest::computePredictionError() {

  // Predict trees in multiple threads
#ifdef OLD_WIN_R_BUILD
  // #nocov start
  progress = 0;
  clock_t start_time = clock();
  clock_t lap_time = clock();
  for (size_t i = 0; i < num_trees; ++i) {
    trees[i]->predict(data.get(), true);
    progress++;
    showProgress("Predicting..", start_time, lap_time);
  }
  // #nocov end
#else
  std::vector<std::thread> threads;
  threads.reserve(num_threads);
  progress = 0;
  for (uint i = 0; i < num_threads; ++i) {
    threads.emplace_back(&Forest::predictTreesInThread, this, i, data.get(), true);
  }
  showProgress("Computing prediction error..", num_trees);
  for (auto &thread : threads) {
    thread.join();
  }

#ifdef R_BUILD
  if (aborted_threads > 0) {
    throw std::runtime_error("User interrupt.");
  }
#endif
#endif

  // Call special function for subclasses
  computePredictionErrorInternal();
}

void Forest::computePermutationImportance() {

  // Compute tree permutation importance in multiple threads
#ifdef OLD_WIN_R_BUILD
  // #nocov start
  progress = 0;
  clock_t start_time = clock();
  clock_t lap_time = clock();

  // Initialize importance and variance
  variable_importance.resize(num_independent_variables, 0);
  std::vector<double> variance;
  if (importance_mode == IMP_PERM_BREIMAN || importance_mode == IMP_PERM_LIAW) {
    variance.resize(num_independent_variables, 0);
  }
  if (importance_mode == IMP_PERM_CASEWISE) {
    variable_importance_casewise.resize(num_independent_variables * num_samples, 0);
  }

  // Compute importance
  for (size_t i = 0; i < num_trees; ++i) {
    trees[i]->computePermutationImportance(variable_importance, variance, variable_importance_casewise);
    progress++;
    showProgress("Computing permutation importance..", start_time, lap_time);
  }

#else
  progress = 0;
#ifdef R_BUILD
  aborted = false;
  aborted_threads = 0;
#endif

  std::vector<std::thread> threads;
  threads.reserve(num_threads);

  // Initialize importance and variance
  std::vector<std::vector<double>> variable_importance_threads(num_threads);
  std::vector<std::vector<double>> variance_threads(num_threads);
  std::vector<std::vector<double>> variable_importance_casewise_threads(num_threads);

  // Compute importance
  for (uint i = 0; i < num_threads; ++i) {
    variable_importance_threads[i].resize(num_independent_variables, 0);
    if (importance_mode == IMP_PERM_BREIMAN || importance_mode == IMP_PERM_LIAW) {
      variance_threads[i].resize(num_independent_variables, 0);
    }
    if (importance_mode == IMP_PERM_CASEWISE) {
      variable_importance_casewise_threads[i].resize(num_independent_variables * num_samples, 0);
    }
    threads.emplace_back(&Forest::computeTreePermutationImportanceInThread, this, i,
        std::ref(variable_importance_threads[i]), std::ref(variance_threads[i]),
        std::ref(variable_importance_casewise_threads[i]));
  }
  showProgress("Computing permutation importance..", num_trees);
  for (auto &thread : threads) {
    thread.join();
  }

#ifdef R_BUILD
  if (aborted_threads > 0) {
    throw std::runtime_error("User interrupt.");
  }
#endif

  // Sum thread importances
  variable_importance.resize(num_independent_variables, 0);
  for (size_t i = 0; i < num_independent_variables; ++i) {
    for (uint j = 0; j < num_threads; ++j) {
      variable_importance[i] += variable_importance_threads[j][i];
    }
  }
  variable_importance_threads.clear();

  // Sum thread variances
  std::vector<double> variance(num_independent_variables, 0);
  if (importance_mode == IMP_PERM_BREIMAN || importance_mode == IMP_PERM_LIAW) {
    for (size_t i = 0; i < num_independent_variables; ++i) {
      for (uint j = 0; j < num_threads; ++j) {
        variance[i] += variance_threads[j][i];
      }
    }
    variance_threads.clear();
  }

  // Sum thread casewise importances
  if (importance_mode == IMP_PERM_CASEWISE) {
    variable_importance_casewise.resize(num_independent_variables * num_samples, 0);
    for (size_t i = 0; i < variable_importance_casewise.size(); ++i) {
      for (uint j = 0; j < num_threads; ++j) {
        variable_importance_casewise[i] += variable_importance_casewise_threads[j][i];
      }
    }
    variable_importance_casewise_threads.clear();
  }
#endif

  for (size_t i = 0; i < variable_importance.size(); ++i) {
    variable_importance[i] /= num_trees;

    // Normalize by variance for scaled permutation importance
    if (importance_mode == IMP_PERM_BREIMAN || importance_mode == IMP_PERM_LIAW) {
      if (variance[i] != 0) {
        variance[i] = variance[i] / num_trees - variable_importance[i] * variable_importance[i];
        variable_importance[i] /= sqrt(variance[i] / num_trees);
      }
    }
  }

  if (importance_mode == IMP_PERM_CASEWISE) {
    for (size_t i = 0; i < variable_importance_casewise.size(); ++i) {
      variable_importance_casewise[i] /= num_trees;
    }
  }
}

std::vector<double> Forest::predictOobWeightedVarU(std::vector<size_t> varU, bool Ucomplement){
  std::vector<double> forest_pred_oob(num_samples, 0);
  std::vector<double> num_oob(num_samples, 0);
  for (size_t i = 0; i < num_trees; ++i) {
    std::vector<double> tree_pred = trees[i] -> predictOobTreeSHAPVarU(varU, Ucomplement, data.get());
    for (size_t ind = 0; ind < num_samples; ++ind) {
      forest_pred_oob[ind] += tree_pred[ind]; 
    }
    std::vector<size_t> oob_sampleIDs = trees[i] -> getOobSampleIDs();
    for (auto & ind : oob_sampleIDs){
      num_oob[ind] += 1;
    }
  }
  for (size_t ind = 0; ind < num_samples; ++ind) {
    forest_pred_oob[ind] = forest_pred_oob[ind]/num_oob[ind]; 
  }
  return(forest_pred_oob);
}

std::map<std::vector<size_t>, double> Forest::getAllVarU(size_t shapley_depth){
  
  std::map<std::vector<size_t>, double> forestVarU;
  std::pair<std::map<std::vector<size_t>, double>, double> treeUpaths;
  double num_paths = 0;
  for (size_t i = 0; i < num_trees; ++i) {
    treeUpaths = trees[i] -> getSirusVarU(shapley_depth);
    std::map<std::vector<size_t>, double> treeU = treeUpaths.first;
    num_paths += treeUpaths.second;
    for (std::map<std::vector<size_t>, double>::iterator it = treeU.begin(); it != treeU.end(); ++it){
      auto it_bool = forestVarU.insert(std::pair<std::vector<size_t>, double> (it->first, it->second));
      if (!it_bool.second){
        it_bool.first->second += it->second;
      }
    }
  }
  
  // for (std::map<std::vector<size_t>, double>::iterator it = forestVarU.begin(); it != forestVarU.end(); ++it){
  //   it->second = it->second / num_paths;
  // }
  
  return(forestVarU);
  
}

std::pair<std::vector<std::vector<std::vector<size_t>>>, std::vector<std::vector<size_t>>> Forest::getVarUsetMC(size_t shapley_depth, size_t num_varU){
  
  std::pair<std::vector<std::vector<std::vector<size_t>>>, std::vector<std::vector<size_t>>> res;
  std::map<std::vector<size_t>, double> UlistAll;
  std::vector<std::vector<size_t>> UlistRaw;
  std::vector<std::vector<size_t>> UlistMC;
  std::vector<size_t> pUfinalMC;
  std::vector<size_t> pU;

  // sample variable subsets U 
  UlistAll = getAllVarU(shapley_depth);
  for (std::map<std::vector<size_t>, double>::iterator it = UlistAll.begin(); it != UlistAll.end(); ++it){
    pU.push_back(static_cast<size_t>(it->second));
    UlistRaw.push_back(it->first);
  }
  std::random_device rd;
  std::mt19937 gen(rd());
  std::discrete_distribution<size_t> distr(pU.begin(), pU.end());
  std::vector<size_t> allk;
  std::vector<size_t> numU;
  for (size_t iter = 0; iter < num_varU; ++iter){
    size_t k = distr(gen);
    auto itU = find(allk.begin(), allk.end(), k);
    if (itU == allk.end()){
      UlistMC.push_back(UlistRaw[k]);
      pUfinalMC.push_back(pU[k]);
      allk.push_back(k);
      numU.push_back(1);
    } else{
      numU[itU - allk.begin()] += 1;
    }
  }
  // std::cout << pUfinalMC.size();
  
  res.first.push_back(UlistMC);
  res.first.push_back(UlistRaw);
  res.second.push_back(pUfinalMC);
  res.second.push_back(numU);
  res.second.push_back(pU);
  
  return(res);
  
}

std::vector<std::vector<size_t>> Forest::getRandVarUset(size_t num_varU){
  
  std::vector<std::vector<size_t>> Ulist;

  // sample variable subsets U 
  for (size_t iter = 0; iter < num_varU; ++iter){
    std::vector<size_t> U;
    size_t dimU = rand() % (num_independent_variables - 1) + 1;
    // for (size_t iterU = 0; iterU < dimU; ++iterU){
    while (U.size() < dimU){
      size_t k = rand() % num_independent_variables;
      if (std::find(U.begin(), U.end(), k) == U.end()){
        U.push_back(k);
      }
    }
    // std::sort(U.begin(), U.end());
    Ulist.push_back(U);
  }

  return(Ulist);
  
}

std::vector<std::vector<std::vector<std::vector<double>>>> Forest::getProjectedLeaves(std::vector<std::vector<size_t>> UlistMC, bool Ucomplement, size_t tree_depth){
  
  std::vector<std::vector<std::vector<std::vector<double>>>> res;
  // res = trees[0] -> predictOobProjectedFastMC(UlistMC, Ucomplement, tree_depth, data.get());
  
  return(res);

}

std::vector<std::vector<size_t>> Forest::getPrunedChildNodes(std::vector<std::vector<size_t>> UlistMC){
  
  std::vector<std::vector<size_t>> res;
  std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>> prunedChildIDs = trees[0] -> getPrunedChildNodes(UlistMC);
  for (auto & childIDs : prunedChildIDs){
    res.push_back(childIDs.first);
  }
  return(res);
  
}

std::vector<std::vector<double>> Forest::predictOobProjectedFastMC(std::vector<std::vector<size_t>> UlistMC, bool Ucomplement, size_t tree_depth){
  
  size_t num_varU = UlistMC.size();
  std::vector<std::pair<std::vector<size_t>, std::vector<double>>> tree_pred;
  std::pair<std::vector<size_t>, std::vector<double>> Upred;
  std::vector<std::vector<double>> sample_size;
  std::vector<std::vector<double>> forest_pred;
  std::vector<double> pred_temp(num_samples, 0);
  std::vector<double> sample_counts(num_samples, 0);
  for (size_t k = 0; k < num_varU; ++k){
    sample_size.push_back(sample_counts);
    forest_pred.push_back(pred_temp);
  }
  
  // std::vector<std::vector<std::vector<double>>> all_pred(num_varU);
  // std::vector<std::vector<double>> vec_temp(num_samples);
  // for (size_t k = 0; k < num_varU; ++k){
  //   all_pred[k] = vec_temp;
  //   for (size_t i; i < num_samples; ++i){
  //     all_pred[k][i] = {};
  //   }
  // }
  
  for (size_t t = 0; t < num_trees; ++t) {
    tree_pred = trees[t] -> predictOobProjectedFastMC(UlistMC, Ucomplement, tree_depth, data.get());
    for (size_t k = 0; k < num_varU; ++k){
      Upred = tree_pred[k];
      pred_temp = Upred.second;
      for (auto & i : Upred.first){
        forest_pred[k][i] += pred_temp[i];
        sample_size[k][i] += 1;
        // all_pred[k][i].push_back(pred_temp[i]);
      }
    }
  }

  for (size_t k = 0; k < num_varU; ++k){
    for (size_t i = 0; i < num_samples; ++i){
      forest_pred[k][i] = forest_pred[k][i] / sample_size[k][i];
    }
  }
  
  // debug
  // std::vector<std::vector<std::vector<double>>> res;
  // std::vector<std::vector<double>> res_temp;
  // for (size_t k = 0; k < num_varU; ++k){
  //   res_temp.push_back(sample_size[k]);
  //   res_temp.push_back(forest_pred[k]);
  //   res.push_back(res_temp);
  //   res_temp.clear();
  // }
  
  // return(sample_size);
  // return(all_pred);
  // return(res);
  return(forest_pred);
  
}

std::map<std::vector<size_t>, std::vector<double>> Forest::predictOobProjectedMC(std::vector<std::vector<size_t>> UlistMC, bool Ucomplement, size_t tree_depth){
  
  std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<double>>> tree_pred;
  std::pair<std::map<std::vector<size_t>, std::vector<double>>::iterator, bool> it_bool;
  std::map<std::vector<size_t>, std::vector<double>> forest_pred;
  std::map<std::vector<size_t>, std::vector<double>> forest_pred_final;
  std::map<std::vector<size_t>, std::vector<size_t>> sample_size;
  std::vector<double> pred_temp;  
  std::vector<std::vector<std::vector<double>>> res;
  std::vector<std::vector<double>> res_temp;
  
  for (size_t t = 0; t < num_trees; ++t) {
    tree_pred = trees[t] -> predictOobProjectedMC(UlistMC, Ucomplement, tree_depth, data.get());
    for (std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<double>>>::iterator it = tree_pred.begin(); it != tree_pred.end(); ++it){
      it_bool = forest_pred.insert(std::pair<std::vector<size_t>, std::vector<double>> (it->first, it->second.second));
      if (it_bool.second){
        std::vector<size_t> sample_counts(num_samples, 0);
        for (auto & i : it->second.first){
          sample_counts[i] = 1;
        }
        sample_size.insert(std::pair<std::vector<size_t>, std::vector<size_t>> (it->first, sample_counts));
      }else{
        pred_temp = it_bool.first->second;
        std::vector<size_t> sample_counts = sample_size[it->first];
        for (auto & i : it->second.first){
          pred_temp[i] += it->second.second[i];
          sample_counts[i] += 1;
        }
        it_bool.first->second = pred_temp;
        sample_size[it->first] = sample_counts;
      }
    }
  }
  
  for (std::map<std::vector<size_t>, std::vector<double>>::iterator it = forest_pred.begin(); it != forest_pred.end(); ++it){
    std::vector<size_t> sample_counts = sample_size[it->first];
    for (size_t i = 0; i < num_samples; ++i){
      it->second[i] = it->second[i] / sample_counts[i]; // check size > 0 ?
    }
  }
  
  for (auto & U : UlistMC){
    auto it = forest_pred.find(U);
    if (it != forest_pred.end()){
      forest_pred_final.insert(std::pair<std::vector<size_t>, std::vector<double>> (U, it->second));
    }
  }
  
  return(forest_pred_final);

}

std::map<std::vector<size_t>, std::vector<double>> Forest::predictOobProjectedDepthAll(size_t depth, size_t tree_depth){
  
  std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<double>>> tree_pred;
  std::pair<std::map<std::vector<size_t>, std::vector<double>>::iterator, bool> it_bool;
  std::map<std::vector<size_t>, std::vector<double>> forest_pred;
  std::map<std::vector<size_t>, std::vector<size_t>> sample_size;
  std::vector<double> pred_temp;  
  std::vector<std::vector<std::vector<double>>> res;
  std::vector<std::vector<double>> res_temp;
  
  for (size_t t = 0; t < num_trees; ++t) {
    tree_pred = trees[t] -> predictOobProjectedDepthAll(depth, tree_depth, data.get());
    for (std::map<std::vector<size_t>, std::pair<std::vector<size_t>, std::vector<double>>>::iterator it = tree_pred.begin(); it != tree_pred.end(); ++it){
      it_bool = forest_pred.insert(std::pair<std::vector<size_t>, std::vector<double>> (it->first, it->second.second));
      if (it_bool.second){
        std::vector<size_t> sample_counts(num_samples, 0);
        for (auto & i : it->second.first){
          sample_counts[i] = 1;
        }
        sample_size.insert(std::pair<std::vector<size_t>, std::vector<size_t>> (it->first, sample_counts));
      }else{
        pred_temp = it_bool.first->second;
        std::vector<size_t> sample_counts = sample_size[it->first];
        for (auto & i : it->second.first){
          pred_temp[i] += it->second.second[i];
          sample_counts[i] += 1;
        }
        it_bool.first->second = pred_temp;
        sample_size[it->first] = sample_counts;
      }
    }
  }
  
  for (std::map<std::vector<size_t>, std::vector<double>>::iterator it = forest_pred.begin(); it != forest_pred.end(); ++it){
    std::vector<size_t> sample_counts = sample_size[it->first];
    for (size_t i = 0; i < num_samples; ++i){
      it->second[i] = it->second[i] / sample_counts[i]; // check size > 0 ?
    }
    // std::vector<double> varIDs(it->first.begin(), it->first.end());
    // res_temp.push_back(varIDs);
    // res_temp.push_back(it->second);
    // res.push_back(res_temp);
    // res_temp.clear();
  }
  
  return(forest_pred);
  
  // std::vector<std::vector<std::vector<std::vector<double>>>> res;
  // for (size_t i = 0; i < num_trees; ++i) {
  //   std::vector<std::vector<std::vector<double>>> tree_pred = trees[i] -> predictOobProjectedDepthAll(depth, data.get());
  //   res.push_back(tree_pred);
  // }
  // 
  // return(res);
  
}

std::vector<std::vector<std::vector<std::vector<size_t>>>> Forest::predictOobProjectedDepthAllPath(size_t depth){
  
  std::vector<std::vector<std::vector<std::vector<size_t>>>> res;
  for (size_t i = 0; i < num_trees; ++i) {
    std::vector<std::vector<std::vector<size_t>>> tree_pred = trees[i] -> predictOobProjectedDepthAllPath(depth, data.get());
    res.push_back(tree_pred);
  }
  
  return(res);
  
}

std::map<std::vector<size_t>, std::vector<double>> Forest::predictOobProjectedDepth(size_t depth){
  std::map<std::vector<size_t>, std::vector<double>> varUpredictions;
  std::vector<double> forest_pred_oob(num_samples, 0);
  std::map<std::vector<size_t>, std::vector<size_t>> varUnumOob;
  std::vector<size_t> num_oob(num_samples, 0);
  std::vector<size_t> varU;
  std::pair<std::map<std::vector<size_t>, std::vector<double>>::iterator, bool> it_pred;
  std::pair<std::map<std::vector<size_t>, std::vector<size_t>>::iterator, bool> it_num;
  for (size_t i = 0; i < num_trees; ++i) {
    std::map<std::vector<size_t>, std::vector<double>> tree_pred = trees[i] -> predictOobProjectedDepth(depth, data.get());
    std::vector<size_t> oob_sampleIDs = trees[i] -> getOobSampleIDs();
    for (std::map<std::vector<size_t>, std::vector<double>>::iterator it = tree_pred.begin(); it != tree_pred.end(); ++it){
      varU = it -> first;
      it_pred = varUpredictions.insert(std::pair<std::vector<size_t>, std::vector<double>> (varU, forest_pred_oob));
      for (size_t ind = 0; ind < num_samples; ++ind) {
        it_pred.first -> second[ind] += it -> second[ind]; 
        // varUpredictions[varU][ind] += it -> second[ind]; 
      }
      it_num = varUnumOob.insert(std::pair<std::vector<size_t>, std::vector<size_t>> (varU, num_oob));
      for (auto & ind : oob_sampleIDs){
        it_num.first -> second[ind] += 1;
        // varUnumOob[varU][ind] += 1;
      }
    }
  }
  for (std::map<std::vector<size_t>, std::vector<double>>::iterator it = varUpredictions.begin(); it != varUpredictions.end(); ++it){
    for (size_t ind = 0; ind < num_samples; ++ind) {
      it -> second[ind] = it -> second[ind] / varUnumOob[it -> first][ind];
    }
  }
  return(varUpredictions);
}

std::vector<double> Forest::predictOobProjectedVarU(std::vector<size_t> varU){
  std::vector<double> forest_pred_oob(num_samples, 0);
  std::vector<double> num_oob(num_samples, 0);
  for (size_t i = 0; i < num_trees; ++i) {
    std::vector<double> tree_pred = trees[i] -> predictOobProjectedVarU(varU, data.get());
    for (size_t ind = 0; ind < num_samples; ++ind) {
      forest_pred_oob[ind] += tree_pred[ind]; 
    }
    std::vector<size_t> oob_sampleIDs = trees[i] -> getOobSampleIDs();
    for (auto & ind : oob_sampleIDs){
      num_oob[ind] += 1;
    }
  }
  for (size_t ind = 0; ind < num_samples; ++ind) {
    forest_pred_oob[ind] = forest_pred_oob[ind]/num_oob[ind]; 
  }
  return(forest_pred_oob);
}

std::vector<double> Forest::predictOobProjectedVarj(size_t varj){
  std::vector<double> forest_pred_oob(num_samples, 0);
  std::vector<double> num_oob(num_samples, 0);
  for (size_t i = 0; i < num_trees; ++i) {
    std::vector<double> tree_pred = trees[i] -> predictOobProjectedVarj(varj, data.get());
    for (size_t ind = 0; ind < num_samples; ++ind) {
      forest_pred_oob[ind] += tree_pred[ind]; 
    }
    std::vector<size_t> oob_sampleIDs = trees[i] -> getOobSampleIDs();
    for (auto & ind : oob_sampleIDs){
      num_oob[ind] += 1;
    }
  }
  for (size_t ind = 0; ind < num_samples; ++ind) {
    forest_pred_oob[ind] = forest_pred_oob[ind]/num_oob[ind]; 
  }
  return(forest_pred_oob);
}

std::vector<std::vector<double>> Forest::predictOobProjectedHighDim(){
  std::vector<std::vector<double>> forest_pred_oob_p;
  std::vector<double> forest_pred_oob(num_samples, 0);
  std::vector<double> num_oob(num_samples, 0);
  for (size_t j = 0; j < num_independent_variables; ++j){
    forest_pred_oob_p.push_back(forest_pred_oob);
  }
  for (size_t i = 0; i < num_trees; ++i) {
    std::vector<std::vector<double>> tree_pred = trees[i] -> predictOobProjectedHighDim(data.get());
    for (size_t j = 0; j < num_independent_variables; ++j){
      for (size_t ind = 0; ind < num_samples; ++ind) {
        forest_pred_oob_p[j][ind] += tree_pred[j][ind]; 
      }
    }
    std::vector<size_t> oob_sampleIDs = trees[i] -> getOobSampleIDs();
    for (auto & ind : oob_sampleIDs){
      num_oob[ind] += 1;
    }
  }
  for (size_t j = 0; j < num_independent_variables; ++j){
    for (size_t ind = 0; ind < num_samples; ++ind) {
      forest_pred_oob_p[j][ind] = forest_pred_oob_p[j][ind]/num_oob[ind]; 
    }
  }
  return(forest_pred_oob_p);
}

std::vector<double> Forest::predictOobPermVarj(size_t j){
  
  std::vector<double> forest_pred_oob(num_samples, 0);
  std::vector<double> num_oob(num_samples, 0);
  for (size_t i = 0; i < num_trees; ++i) {
    std::vector<double> tree_pred = trees[i] -> predictOobPermVarj(j, data.get());
    for (size_t ind = 0; ind < num_samples; ++ind) {
      forest_pred_oob[ind] += tree_pred[ind]; 
    }
    std::vector<size_t> oob_sampleIDs = trees[i] -> getOobSampleIDs();
    for (auto & ind : oob_sampleIDs){
      num_oob[ind] += 1;
    }
  }
  for (size_t ind = 0; ind < num_samples; ++ind) {
    forest_pred_oob[ind] = forest_pred_oob[ind]/num_oob[ind]; 
  }
  return(forest_pred_oob);
}

double Forest::computeVarianceY(){
  
  std::vector<double> y;
  for (size_t ind = 0; ind < num_samples; ++ind) {
    y.push_back(data->get_y(ind, 0));
  }
  double meanY = std::accumulate(y.begin(), y.end(), 0.0);
  meanY = meanY/num_samples;
  double varY = 0;
  for (auto & z : y){
    varY += (z - meanY)*(z - meanY);
  }
  varY = varY/(num_samples-1);
  
  return(varY);
  
}

std::vector<double> Forest::computeVarianceWeightedVarU(std::vector<std::vector<size_t>> UlistMC, bool Ucomplement){
  
  // double var = 0;
  // double var2 = 0;
  // std::vector<double> pred = predictOobWeightedVarU(varU);
  // double meanPred = std::accumulate(pred.begin(), pred.end(), 0.0);
  // meanPred = meanPred/num_samples;
  // for (size_t ind = 0; ind < num_samples; ++ind) {
  //   double y = data->get_y(ind, 0);
  //   var += (y - pred[ind])*(y - pred[ind]);
  //   var2 += (pred[ind] - meanPred)*(pred[ind] - meanPred);
  // }
  // var = var/num_samples;
  // var2 = var2/num_samples;
  // std::vector<double> res = {var, var2};
  // 
  // return(res);
  
  size_t num_varU = UlistMC.size();
  std::vector<std::vector<double>> varUpredictions;
  for (auto & varU : UlistMC){
    std::vector<double> pred = predictOobWeightedVarU(varU, Ucomplement);
    varUpredictions.push_back(pred);
  }
  std::vector<double> varUvariance;
  std::vector<double> predictions;
  double variance;
  size_t sample_size;
  std::vector<double> y;
  for (size_t i = 0; i < num_samples; ++i) {
    y.push_back(data -> get_y(i, 0));
  }
  double varY = computeVarianceY();
  
  for (size_t k = 0; k < num_varU; ++k){
    predictions = varUpredictions[k];
    variance = 0;
    sample_size = 0;
    for (size_t i = 0; i < num_samples; ++i) {
      if (!std::isnan(predictions[i])){
        variance += (y[i] - predictions[i])*(y[i] - predictions[i]);
        sample_size += 1;
      }
    }
    variance = variance / sample_size;
    variance  = variance / varY;
    varUvariance.push_back(variance);
  }
  
  return(varUvariance);
  
}

std::vector<double> Forest::computeVarianceProjectedFastMC(std::vector<std::vector<size_t>> UlistMC, bool Ucomplement, size_t tree_depth){
  
  size_t num_varU = UlistMC.size();
  std::vector<std::vector<double>> varUpredictions;
  varUpredictions = predictOobProjectedFastMC(UlistMC, Ucomplement, tree_depth);
  std::vector<double> varUvariance;
  std::vector<double> predictions;
  double variance;
  size_t sample_size;
  std::vector<double> y;
  for (size_t i = 0; i < num_samples; ++i) {
    y.push_back(data -> get_y(i, 0));
  }
  double varY = computeVarianceY();

  for (size_t k = 0; k < num_varU; ++k){
    predictions = varUpredictions[k];
    variance = 0;
    sample_size = 0;
    for (size_t i = 0; i < num_samples; ++i) {
      if (!std::isnan(predictions[i])){
        variance += (y[i] - predictions[i])*(y[i] - predictions[i]);
        sample_size += 1;
      }
    }
    variance = variance / sample_size;
    variance  = variance / varY;
    varUvariance.push_back(variance);
  }
  
  return(varUvariance);
  
}  

std::map<std::vector<size_t>, double> Forest::computeVarianceProjectedMC(std::vector<std::vector<size_t>> UlistMC, bool Ucomplement, size_t tree_depth){
  
  std::map<std::vector<size_t>, std::vector<double>> varUpredictions;
  varUpredictions = predictOobProjectedMC(UlistMC, Ucomplement, tree_depth);
  std::map<std::vector<size_t>, double> varUvariance;
  std::vector<double> predictions;
  double variance;
  size_t sample_size;
  std::vector<double> y;
  for (size_t i = 0; i < num_samples; ++i) {
    y.push_back(data -> get_y(i, 0));
  }
  double varY = computeVarianceY();
  
  for (std::map<std::vector<size_t>, std::vector<double>>::iterator it = varUpredictions.begin(); it != varUpredictions.end(); ++it){
    predictions = it -> second;
    variance = 0;
    sample_size = 0;
    for (size_t i = 0; i < num_samples; ++i) {
      if (!std::isnan(predictions[i])){
        variance += (y[i] - predictions[i])*(y[i] - predictions[i]);
        sample_size += 1;
      }
    }
    variance = variance / sample_size;
    varUvariance[it -> first] = variance / varY;
  }
  
  return(varUvariance);
  
}  

std::map<std::vector<size_t>, double> Forest::computeVarianceProjectedDepthAll(size_t depth, size_t tree_depth){
  
  std::map<std::vector<size_t>, std::vector<double>> varUpredictions;
  varUpredictions = predictOobProjectedDepthAll(depth, tree_depth);
  std::map<std::vector<size_t>, double> varUvariance;
  std::vector<double> predictions;
  double variance;
  size_t sample_size;
  std::vector<double> y;
  for (size_t i = 0; i < num_samples; ++i) {
    y.push_back(data -> get_y(i, 0));
  }
  double varY = computeVarianceY();
  
  for (std::map<std::vector<size_t>, std::vector<double>>::iterator it = varUpredictions.begin(); it != varUpredictions.end(); ++it){
    predictions = it -> second;
    variance = 0;
    sample_size = 0;
    for (size_t i = 0; i < num_samples; ++i) {
      if (!std::isnan(predictions[i])){
        variance += (y[i] - predictions[i])*(y[i] - predictions[i]);
        sample_size += 1;
      }
    }
    variance = variance / sample_size;
    varUvariance[it -> first] = variance / varY;
  }
  
  return(varUvariance);
  
}  

std::map<std::vector<size_t>, double> Forest::computeVarianceProjectedDepthPath(size_t depth){

  std::map<std::vector<size_t>, std::vector<double>> varUpredictions;
  varUpredictions = predictOobProjectedDepth(depth);
  std::map<std::vector<size_t>, double> varUvariance;
  std::vector<double> predictions;
  double variance;
  size_t sample_size;
  std::vector<double> y;
  for (size_t i = 0; i < num_samples; ++i) {
    y.push_back(data -> get_y(i, 0));
  }
  double varY = computeVarianceY();
  
  for (std::map<std::vector<size_t>, std::vector<double>>::iterator it = varUpredictions.begin(); it != varUpredictions.end(); ++it){
    predictions = it -> second;
    variance = 0;
    sample_size = 0;
    for (size_t i = 0; i < num_samples; ++i) {
      if (!std::isnan(predictions[i])){
        variance += (y[i] - predictions[i])*(y[i] - predictions[i]);
        sample_size += 1;
      }
    }
    variance = variance / sample_size;
    varUvariance[it -> first] = variance / varY;
  }
  
  return(varUvariance);
  
}  
  
std::vector<double> Forest::computeVarianceProjectedVarU(std::vector<size_t> varU){
  
  double var = 0;
  double var2 = 0;
  std::vector<double> pred = predictOobProjectedVarU(varU);
  double meanPred = std::accumulate(pred.begin(), pred.end(), 0.0);
  meanPred = meanPred/num_samples;
  for (size_t ind = 0; ind < num_samples; ++ind) {
    double y = data->get_y(ind, 0);
    var += (y - pred[ind])*(y - pred[ind]);
    var2 += (pred[ind] - meanPred)*(pred[ind] - meanPred);
  }
  var = var/num_samples;
  var2 = var2/num_samples;
  std::vector<double> res = {var, var2};
  
  return(res);
  
}

double Forest::computeVarianceProjectedVarj(size_t varj){
  
  double var = 0;
  std::vector<double> pred = predictOobProjectedVarj(varj);
  for (size_t ind = 0; ind < num_samples; ++ind) {
    double y = data->get_y(ind, 0);
    var += (y - pred[ind])*(y - pred[ind]);
  }
  var = var/num_samples;
  
  return(var);
  
}

std::vector<double> Forest::computeVarianceProjectedHighDim(){
  
  std::vector<std::vector<double>> pred = predictOobProjectedHighDim();
  std::vector<double> var(num_independent_variables, 0);
  for (size_t ind = 0; ind < num_samples; ++ind) {
    double y = data->get_y(ind, 0);
    for (size_t j = 0; j < num_independent_variables; ++j){
      var[j] += (y - pred[j][ind])*(y - pred[j][ind]);
    }
  }
  for (size_t j = 0; j < num_independent_variables; ++j){
    var[j] = var[j]/num_samples;
  }
  
  return(var);
  
}

double Forest::computeVariancePermVarj(size_t j){
  
  double var = 0;
  std::vector<double> pred = predictOobPermVarj(j);
  for (size_t ind = 0; ind < num_samples; ++ind) {
    double y = data->get_y(ind, 0);
    var += (y - pred[ind])*(y - pred[ind]);
  }
  var = var/num_samples;
  
  return(var);
  
}

void Forest::generateAllVarU(){
  
  // std::vector<std::vector<size_t>> treeVarU;
  // for (size_t i = 0; i < num_trees; ++i){
  //   treeVarU = trees[i] -> getU();
  //   std::map<std::vector<size_t>, int>::iterator it;
  //   for (auto & x : treeVarU){
  //     //allVarU.push_back(x);
  //     it = forest_varU.find(x);
  //     if (it == forest_varU.end()){
  //       forest_varU[x] = 1;
  //     }else{
  //       forest_varU[x] += 1;
  //     }
  //   }
  // }
  // 
  // std::vector<int> probU = getVarUProba();
  // std::nth_element(probU.begin(), probU.begin() + 100, probU.end(), std::greater<int>());
  // 
  // for (std::map<std::vector<size_t>, int>::iterator it = forest_varU.begin(); it != forest_varU.end(); ++it){
  //   if (it->second >= probU[100]){
  //     allVarU.push_back(it->first);
  //   }
  // }
  // // test
  // allVarU.push_back({11});
  
  for (size_t j = 0; j < num_independent_variables; ++j){
      std::vector<size_t> varU = {j};
      allVarU.push_back(varU);
  }
  std::vector<std::vector<size_t>> varUtemp;
  std::vector<std::vector<size_t>> varUnext = allVarU;
  std::vector<size_t> Utemp;
  int c = 2;
  while (c <= num_independent_variables){
    c += 1;
    for (auto & U : varUnext){
      for (size_t j = 0; j < num_independent_variables; ++j){
        if (std::find(U.begin(), U.end(), j) == U.end()){
          Utemp = U;
          Utemp.push_back(j);
          std::sort(Utemp.begin(), Utemp.end());
          if (std::find(varUtemp.begin(), varUtemp.end(), Utemp) == varUtemp.end()){
            varUtemp.push_back(Utemp);
          }
        }
      }
    }
    allVarU.insert(allVarU.end(), varUtemp.begin(), varUtemp.end());
    varUnext = varUtemp;
    varUtemp.clear();
  }
  
}

std::vector<std::vector<double>> Forest::getAllCondVarWeighted(){
  
  generateAllVarU();
  
  std::vector<std::vector<double>> allCondVar;
  // std::vector<double> condVarU;
  // double varY = computeVarianceY();
  // for (auto & varU : allVarU){
  //   condVarU = computeVarianceWeightedVarU(varU);///varY;
  //   condVarU[0] = condVarU[0]/varY;
  //   condVarU[1] = condVarU[1]/varY;
  //   allCondVar.push_back(condVarU);
  // }
  return(allCondVar);
  
}

std::vector<std::vector<double>> Forest::getAllCondVar(){
  
  generateAllVarU();
  
  std::vector<std::vector<double>> allCondVar;
  std::vector<double> condVarU;
  double varY = computeVarianceY();
  for (auto & varU : allVarU){
    condVarU = computeVarianceProjectedVarU(varU);///varY;
    condVarU[0] = condVarU[0]/varY;
    condVarU[1] = condVarU[1]/varY;
    allCondVar.push_back(condVarU);
  }
  return(allCondVar);
  
}

std::vector<double> Forest::getSobolMDA(){
  
  std::vector<double> SMDA;
  double varianceAll;
  double SMDAvarj;
  double varY = computeVarianceY();
  varianceAll = computeVarianceProjectedVarj(num_independent_variables);
  for (size_t j = 0; j < num_independent_variables; j++){
    SMDAvarj = (computeVarianceProjectedVarj(j) - varianceAll)/varY;
    SMDA.push_back(SMDAvarj);
  }
  return(SMDA);
  
}

std::vector<double> Forest::getSobolMDAHighDim(){
  
  double varianceAll;
  double SMDAvarj;
  double varY = computeVarianceY();
  varianceAll = computeVarianceProjectedVarj(num_independent_variables);
  std::vector<double> SMDA = computeVarianceProjectedHighDim();
  for (size_t j = 0; j < num_independent_variables; j++){
    SMDA[j] = (SMDA[j] - varianceAll)/varY;
  }
  return(SMDA);
  
}

std::vector<double> Forest::getIKMDA(){
  
  std::vector<double> MDA;
  double varianceAll;
  double MDAvarj;
  double varY = computeVarianceY();
  varianceAll = computeVarianceProjectedVarj(num_independent_variables);
  for (size_t j = 0; j < num_independent_variables; j++){
    MDAvarj = (computeVariancePermVarj(j) - varianceAll)/varY;
    MDA.push_back(MDAvarj);
  }
  return(MDA);
  
}


#ifndef OLD_WIN_R_BUILD
void Forest::growTreesInThread(uint thread_idx, std::vector<double>* variable_importance) {
  if (thread_ranges.size() > thread_idx + 1) {
    for (size_t i = thread_ranges[thread_idx]; i < thread_ranges[thread_idx + 1]; ++i) {
      trees[i]->grow(variable_importance);

      // Check for user interrupt
#ifdef R_BUILD
      if (aborted) {
        std::unique_lock<std::mutex> lock(mutex);
        ++aborted_threads;
        condition_variable.notify_one();
        return;
      }
#endif

      // Increase progress by 1 tree
      std::unique_lock<std::mutex> lock(mutex);
      ++progress;
      condition_variable.notify_one();
    }
  }
}

void Forest::predictTreesInThread(uint thread_idx, const Data* prediction_data, bool oob_prediction) {
  if (thread_ranges.size() > thread_idx + 1) {
    for (size_t i = thread_ranges[thread_idx]; i < thread_ranges[thread_idx + 1]; ++i) {
      trees[i]->predict(prediction_data, oob_prediction);

      // Check for user interrupt
#ifdef R_BUILD
      if (aborted) {
        std::unique_lock<std::mutex> lock(mutex);
        ++aborted_threads;
        condition_variable.notify_one();
        return;
      }
#endif

      // Increase progress by 1 tree
      std::unique_lock<std::mutex> lock(mutex);
      ++progress;
      condition_variable.notify_one();
    }
  }
}

void Forest::predictInternalInThread(uint thread_idx) {
  // Create thread ranges
  std::vector<uint> predict_ranges;
  equalSplit(predict_ranges, 0, num_samples - 1, num_threads);

  if (predict_ranges.size() > thread_idx + 1) {
    for (size_t i = predict_ranges[thread_idx]; i < predict_ranges[thread_idx + 1]; ++i) {
      predictInternal(i);

      // Check for user interrupt
#ifdef R_BUILD
      if (aborted) {
        std::unique_lock<std::mutex> lock(mutex);
        ++aborted_threads;
        condition_variable.notify_one();
        return;
      }
#endif

      // Increase progress by 1 tree
      std::unique_lock<std::mutex> lock(mutex);
      ++progress;
      condition_variable.notify_one();
    }
  }
}

void Forest::computeTreePermutationImportanceInThread(uint thread_idx, std::vector<double>& importance,
    std::vector<double>& variance, std::vector<double>& importance_casewise) {
  if (thread_ranges.size() > thread_idx + 1) {
    for (size_t i = thread_ranges[thread_idx]; i < thread_ranges[thread_idx + 1]; ++i) {
      trees[i]->computePermutationImportance(importance, variance, importance_casewise);

      // Check for user interrupt
#ifdef R_BUILD
      if (aborted) {
        std::unique_lock<std::mutex> lock(mutex);
        ++aborted_threads;
        condition_variable.notify_one();
        return;
      }
#endif

      // Increase progress by 1 tree
      std::unique_lock<std::mutex> lock(mutex);
      ++progress;
      condition_variable.notify_one();
    }
  }
}
#endif

// #nocov start
void Forest::loadFromFile(std::string filename) {
  if (verbose_out)
    *verbose_out << "Loading forest from file " << filename << "." << std::endl;

  // Open file for reading
  std::ifstream infile;
  infile.open(filename, std::ios::binary);
  if (!infile.good()) {
    throw std::runtime_error("Could not read from input file: " + filename + ".");
  }

  // Skip dependent variable names (already read)
  uint num_dependent_variables;
  infile.read((char*) &num_dependent_variables, sizeof(num_dependent_variables));
  for (size_t i = 0; i < num_dependent_variables; ++i) {
    size_t length;
    infile.read((char*) &length, sizeof(size_t));
    infile.ignore(length);
  }

  // Read num_trees
  infile.read((char*) &num_trees, sizeof(num_trees));

  // Read is_ordered_variable
  readVector1D(data->getIsOrderedVariable(), infile);

  // Read tree data. This is different for tree types -> virtual function
  loadFromFileInternal(infile);

  infile.close();

  // Create thread ranges
  equalSplit(thread_ranges, 0, num_trees - 1, num_threads);
}

void Forest::loadDependentVariableNamesFromFile(std::string filename) {

  // Open file for reading
  std::ifstream infile;
  infile.open(filename, std::ios::binary);
  if (!infile.good()) {
    throw std::runtime_error("Could not read from input file: " + filename + ".");
  }

  // Read dependent variable names
  dependent_variable_names.clear();
  uint num_dependent_variables = 0;
  infile.read((char*) &num_dependent_variables, sizeof(num_dependent_variables));
  for (size_t i = 0; i < num_dependent_variables; ++i) {
    size_t length;
    infile.read((char*) &length, sizeof(size_t));
    char* temp = new char[length + 1];
    infile.read((char*) temp, length * sizeof(char));
    temp[length] = '\0';
    dependent_variable_names.push_back(temp);
    delete[] temp;
  }

  infile.close();
}

std::unique_ptr<Data> Forest::loadDataFromFile(const std::string& data_path) {
  std::unique_ptr<Data> result { };
  switch (memory_mode) {
  case MEM_DOUBLE:
    result = make_unique<DataDouble>();
    break;
  case MEM_FLOAT:
    result = make_unique<DataFloat>();
    break;
  case MEM_CHAR:
    result = make_unique<DataChar>();
    break;
  }

  if (verbose_out)
    *verbose_out << "Loading input file: " << data_path << "." << std::endl;
  bool found_rounding_error = result->loadFromFile(data_path, dependent_variable_names);
  if (found_rounding_error && verbose_out) {
    *verbose_out << "Warning: Rounding or Integer overflow occurred. Use FLOAT or DOUBLE precision to avoid this."
        << std::endl;
  }
  return result;
}
// #nocov end

void Forest::setSplitWeightVector(std::vector<std::vector<double>>& split_select_weights) {

  // Size should be 1 x num_independent_variables or num_trees x num_independent_variables
  if (split_select_weights.size() != 1 && split_select_weights.size() != num_trees) {
    throw std::runtime_error("Size of split select weights not equal to 1 or number of trees.");
  }

  // Reserve space
  size_t num_weights = num_independent_variables;
  if (importance_mode == IMP_GINI_CORRECTED) {
    num_weights = 2 * num_independent_variables;
  }
  if (split_select_weights.size() == 1) {
    this->split_select_weights[0].resize(num_weights);
  } else {
    this->split_select_weights.clear();
    this->split_select_weights.resize(num_trees, std::vector<double>(num_weights));
  }

  // Split up in deterministic and weighted variables, ignore zero weights
  for (size_t i = 0; i < split_select_weights.size(); ++i) {
    size_t num_zero_weights = 0;

    // Size should be 1 x num_independent_variables or num_trees x num_independent_variables
    if (split_select_weights[i].size() != num_independent_variables) {
      throw std::runtime_error("Number of split select weights not equal to number of independent variables.");
    }

    for (size_t j = 0; j < split_select_weights[i].size(); ++j) {
      double weight = split_select_weights[i][j];

      if (weight == 0) {
        ++num_zero_weights;
      } else if (weight < 0 || weight > 1) {
        throw std::runtime_error("One or more split select weights not in range [0,1].");
      } else {
        this->split_select_weights[i][j] = weight;
      }
    }

    // Copy weights for corrected impurity importance
    if (importance_mode == IMP_GINI_CORRECTED) {
      std::vector<double>* sw = &(this->split_select_weights[i]);
      std::copy_n(sw->begin(), num_independent_variables, sw->begin() + num_independent_variables);
    }

    if (num_weights - num_zero_weights < mtry) {
      throw std::runtime_error("Too many zeros in split select weights. Need at least mtry variables to split at.");
    }
  }
}

void Forest::setAlwaysSplitVariables(const std::vector<std::string>& always_split_variable_names) {

  deterministic_varIDs.reserve(num_independent_variables);

  for (auto& variable_name : always_split_variable_names) {
    size_t varID = data->getVariableID(variable_name);
    deterministic_varIDs.push_back(varID);
  }

  if (deterministic_varIDs.size() + this->mtry > num_independent_variables) {
    throw std::runtime_error(
        "Number of variables to be always considered for splitting plus mtry cannot be larger than number of independent variables.");
  }

  // Also add variables for corrected impurity importance
  if (importance_mode == IMP_GINI_CORRECTED) {
    size_t num_deterministic_varIDs = deterministic_varIDs.size();
    for (size_t k = 0; k < num_deterministic_varIDs; ++k) {
      deterministic_varIDs.push_back(k + num_independent_variables);
    }
  }
}

#ifdef OLD_WIN_R_BUILD
// #nocov start
void Forest::showProgress(std::string operation, clock_t start_time, clock_t& lap_time) {

  // Check for user interrupt
  if (checkInterrupt()) {
    throw std::runtime_error("User interrupt.");
  }

  double elapsed_time = (clock() - lap_time) / CLOCKS_PER_SEC;
  if (elapsed_time > STATUS_INTERVAL) {
    double relative_progress = (double) progress / (double) num_trees;
    double time_from_start = (clock() - start_time) / CLOCKS_PER_SEC;
    uint remaining_time = (1 / relative_progress - 1) * time_from_start;
    if (verbose_out) {
      *verbose_out << operation << " Progress: " << round(100 * relative_progress)
      << "%. Estimated remaining time: " << beautifyTime(remaining_time) << "." << std::endl;
    }
    lap_time = clock();
  }
}
// #nocov end
#else
void Forest::showProgress(std::string operation, size_t max_progress) {
  using std::chrono::steady_clock;
  using std::chrono::duration_cast;
  using std::chrono::seconds;

  steady_clock::time_point start_time = steady_clock::now();
  steady_clock::time_point last_time = steady_clock::now();
  std::unique_lock<std::mutex> lock(mutex);

  // Wait for message from threads and show output if enough time elapsed
  while (progress < max_progress) {
    condition_variable.wait(lock);
    seconds elapsed_time = duration_cast<seconds>(steady_clock::now() - last_time);

    // Check for user interrupt
#ifdef R_BUILD
    if (!aborted && checkInterrupt()) {
      aborted = true;
    }
    if (aborted && aborted_threads >= num_threads) {
      return;
    }
#endif

    if (progress > 0 && elapsed_time.count() > STATUS_INTERVAL) {
      double relative_progress = (double) progress / (double) max_progress;
      seconds time_from_start = duration_cast<seconds>(steady_clock::now() - start_time);
      uint remaining_time = (1 / relative_progress - 1) * time_from_start.count();
      if (verbose_out) {
        *verbose_out << operation << " Progress: " << round(100 * relative_progress) << "%. Estimated remaining time: "
            << beautifyTime(remaining_time) << "." << std::endl;
      }
      last_time = steady_clock::now();
    }
  }
}
#endif

} // namespace shaff
