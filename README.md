## SHAFF: SHApley eFfects via random Forests
C.Benard

The package shaff implements the SHAFF algorithm: a fast and accurate Shapley effect estimate, based on random forests.

shaff is a fork from the project ranger, a fast random forest implementation (https://github.com/imbs-hl/ranger).


### Installation

To recompile from source, run the following R command:
`install.package("release/shaff_0.1.1.tar.gz")`

A pre-compiled binary is available for windows (shaff_0.1.1.zip).


### References

* C. Bénard, G. Biau, S. Da Veiga, and E. Scornet (2022). SHAFF: Fast and consistent SHApley eFfect estimates via random Forests. In Proceedings of the 25th International Conference on Artificial Intelligence and Statistics, PMLR 151:5563-5582.
* Wright, M. N. & Ziegler, A. (2017). ranger: A fast implementation of random forests for high dimensional data in C++ and R. J Stat Softw 77:1-17. https://doi.org/10.18637/jss.v077.i01.


